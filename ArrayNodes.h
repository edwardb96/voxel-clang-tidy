//===--- ArrayNodes.h - clang-tidy----------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_ARRAY_NODES_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_ARRAY_NODES_H
#include "ExtractorBase.h"
#include "GetEndLoc.h"
#include "LinearSearchForConst.h"
#include "MakeIfAllHaveValues.h"
#include "clang/AST/TypeLoc.h"
#include "llvm/ADT/Optional.h"
#include <type_traits>

namespace clang {
namespace ast_matchers {
internal::Matcher<TypeLoc> findAllNonPointerConstArrayLocs();
} // namespace ast_matchers
namespace tidy {
namespace voxel {
template <typename ElementContext> struct ArrayNodes {
  ArrayNodes(ElementContext Element, ArrayTypeLoc ArrayLoc)
      : Element(std::move(Element)), ArrayLoc(ArrayLoc) {}

  ElementContext Element;
  ArrayTypeLoc ArrayLoc;
};

template <typename ElementContext, typename ElementContextExtractor>
class ExtractArrayNodes {
public:
  ExtractArrayNodes(ElementContextExtractor const &ExtractElementNodes)
      : ExtractElementNodes(ExtractElementNodes) {}

  llvm::Optional<ArrayNodes<ElementContext>>
  operator()(const ast_matchers::BoundNodes &BoundNodes) const {
    return MakeIfAllHaveValues<ArrayNodes<ElementContext>>(
        ExtractElementNodes(BoundNodes),
        Extract<ArrayTypeLoc>(BoundNodes, "array"));
  }

private:
  ElementContextExtractor ExtractElementNodes;
};

template <typename ElementContextExtractor,
          typename ElementContext = ResultOfExtractor<ElementContextExtractor>>
ExtractArrayNodes<ElementContext, ElementContextExtractor>
MakeExtractArrayNodes(ElementContextExtractor const &ExtractElementNodes) {
  return ExtractArrayNodes<ElementContext, ElementContextExtractor>(
      ExtractElementNodes);
}

template <typename ElementContext>
const TypeLoc &TypeLocToFix(ArrayNodes<ElementContext> const &Nodes) {
  return TypeLocToFix(Nodes.Element);
}

template <typename ElementContext>
auto LeftParentContext(ArrayNodes<ElementContext> const &Nodes)
    -> decltype(ParentContext(Nodes.Element)) {
  return ParentContext(Nodes.Element);
}

inline LeftToRightSourceTraversal<RightOfNode<TypeLoc>, SourceLocation>
RightQualifierSpace(TypeLoc ElementLoc, ArrayTypeLoc const &Ancestor) {
  return LeftToRight(RightOf(ElementLoc), Ancestor.getLBracketLoc());
}

template <typename ElementContext>
ArrayTypeLoc RightParentContext(ArrayNodes<ElementContext> const &Nodes) {
  return Nodes.ArrayLoc;
}

template <typename ElementContext, typename LinearSearcher>
llvm::Optional<Token> FindRightConst(LinearSearcher const &LinearSearchForConst,
                                     ArrayNodes<ElementContext> const &Nodes) {
  return LinearSearchForConst(
      RightQualifierSpace(TypeLocToFix(Nodes.Element), Nodes.ArrayLoc));
}
} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_ARRAY_NODES_H
