#include "DeclaredTypeNodes.h"
#include "ASTMatchers.h"
#include "LinearSearchForConst.h"
#include "SourceLocations.h"
#include "TokenPredicates.h"

namespace clang {
namespace ast_matchers {
internal::Matcher<Decl> varDeclOfTypeContainingConst() {
  return varDecl(
             declaredType(anyOf(isConstNonPointerLoc(), findConstRefereeLocs(),
                                findAllNonPointerConstArrayLocs(),
                                findAllNonPointerConstPointeeLocs())))
      .bind("decl");
}

internal::Matcher<Decl> fieldDeclOfTypeContainingConst() {
  return fieldDecl(
             declaredType(anyOf(isConstNonPointerLoc(), findConstRefereeLocs(),
                                findAllNonPointerConstArrayLocs(),
                                findAllNonPointerConstPointeeLocs())))
      .bind("decl");
}

internal::Matcher<Decl> usingDeclOfTypeContainingConst() {
  return typeAliasDecl(
             declaredType(anyOf(isConstNonPointerLoc(), findConstRefereeLocs(),
                                findAllNonPointerConstPointeeLocs())))
      .bind("decl");
}

internal::Matcher<Decl> typedefDeclOfTypeContainingConst() {
  return typedefDecl(
             declaredType(anyOf(isConstNonPointerLoc(), findConstRefereeLocs(),
                                findAllNonPointerConstPointeeLocs())))
      .bind("decl");
}
} // namespace ast_matchers
namespace tidy {
namespace voxel {

BeforeInit::BeforeInit(VarDecl const &VarNode) : VarNode(VarNode) {}

SourceLocation BeforeInit::getSourceLocation(ASTContext const &Context) const {
  if (VarNode.hasInit())
    return VarNode.getInit()->getLocStart();
  else
    return EndCharLoc(Context, VarNode);
}

BeforeInClassInit::BeforeInClassInit(FieldDecl const &FieldNode)
    : FieldNode(FieldNode) {}

SourceLocation
BeforeInClassInit::getSourceLocation(ASTContext const &Context) const {
  if (FieldNode.hasInClassInitializer())
    return FieldNode.getInClassInitializer()->getLocStart();
  else
    return EndCharLoc(Context, FieldNode);
}

LeftToRightSourceTraversal<RightOfNode<TypeLoc>, BeforeInClassInit>
RightQualifierSpace(TypeLoc ArgumentLoc, FieldDecl const &Ancestor) {
  return LeftToRight(RightOf(ArgumentLoc), BeforeInClassInit(Ancestor));
}

LeftToRightSourceTraversal<RightOfNode<TypeLoc>, BeforeInit>
RightQualifierSpace(TypeLoc ArgumentLoc, VarDecl const &Ancestor) {
  return LeftToRight(RightOf(ArgumentLoc), BeforeInit(Ancestor));
}
} // namespace voxel
} // namespace tidy
} // namespace clang
