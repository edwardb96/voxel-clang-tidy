//===--- ReturnTypeNodes.h - clang-tidy------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_RETURN_TYPE_NODES_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_RETURN_TYPE_NODES_H
#include "ASTMatchers.h"
#include "BadConstFinder.h"
#include "ExtractorBase.h"
#include "GetEndLoc.h"
#include "MakeIfAllHaveValues.h"
#include "PointerNodes.h"
#include "clang/AST/TypeLoc.h"
#include "llvm/ADT/Optional.h"
#include <cassert>

namespace clang {
namespace ast_matchers {
internal::Matcher<Decl> functionDeclReturnTypeContainingConst();
internal::Matcher<Stmt> lambdaExprReturnTypeContainingConst();
} // namespace ast_matchers
namespace tidy {
namespace voxel {
template <typename ReturnTypeContext> struct ReturnTypeNodes {
  ReturnTypeNodes(NodeStorage<ReturnTypeContext> Context, TypeLoc ReturnTypeLoc)
      : Context(Context), ReturnTypeLoc(ReturnTypeLoc) {
    assert(!ReturnTypeLoc.isNull());
  }

  NodeStorage<ReturnTypeContext> Context;
  TypeLoc ReturnTypeLoc;
};

template <typename ReturnTypeContext>
const TypeLoc &TypeLocToFix(ReturnTypeNodes<ReturnTypeContext> const &Nodes) {
  return Nodes.ReturnTypeLoc;
}

template <typename ReturnTypeContext>
const ReturnTypeContext &
ParentContext(ReturnTypeNodes<ReturnTypeContext> const &Nodes) {
  return get(Nodes.Context);
}

using FunctionReturnTypeNodes = ReturnTypeNodes<FunctionDecl>;
class ExtractFunctionReturnTypeNodes {
public:
  llvm::Optional<FunctionReturnTypeNodes>
  operator()(const ast_matchers::BoundNodes &BoundNodes) const;
};

using LambdaReturnTypeNodes = ReturnTypeNodes<LambdaExpr>;
class ExtractLambdaReturnTypeNodes {
public:
  llvm::Optional<LambdaReturnTypeNodes>
  operator()(const ast_matchers::BoundNodes &BoundNodes)const;
};

template <typename ReturnTypeContext, typename LinearSearcher>
llvm::Optional<Token>
FindLeftConst(LinearSearcher const &LinearSearchForConst,
              ReturnTypeNodes<ReturnTypeContext> const &Nodes) {
  return LinearSearchForConst(LeftQualifierSpace(Nodes),
                              StoppingIf(&IsLeftReturnTypeEdge));
}

template <typename ReturnTypeContext, typename LinearSearcher>
llvm::Optional<Token>
FindRightConst(LinearSearcher const &LinearSearchForConst,
               ReturnTypeNodes<ReturnTypeContext> const &Nodes) {
  return LinearSearchForConst(RightQualifierSpace(Nodes),
                              StoppingIf(&IsRightReturnTypeEdge));
}

template <typename ReturnTypeContext, typename LinearSearcher>
llvm::Optional<Token>
FindLeftConst(LinearSearcher const &LinearSearchForConst,
              PointerNodes<ReturnTypeNodes<ReturnTypeContext>> const &Nodes) {
  return LinearSearchForConst(LeftQualifierSpace(Nodes),
                              StoppingIf(&IsLeftReturnTypeEdge));
}

} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_RETURN_TYPE_NODES_H
