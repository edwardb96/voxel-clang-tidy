#include "GenericVariableNamesCheck.h"
#include "clang/AST/ASTContext.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"

using namespace clang::ast_matchers;

namespace clang {
namespace tidy {
namespace voxel {

void GenericVariableNamesCheck::registerMatchers(MatchFinder *Finder) {
  // FIXME: Add matchers.
  Finder->addMatcher(varDecl().bind("var"), this);
  Finder->addMatcher(fieldDecl().bind("field"), this);
}

llvm::Optional<llvm::StringRef> GenericVariableNamesCheck::nameIfFieldDecl(const MatchFinder::MatchResult &Result) {
  auto* MatchedDecl = Result.Nodes.getNodeAs<FieldDecl>("field");
  if(MatchedDecl != nullptr) {
    return MatchedDecl->getName();
  } else {
    return llvm::Optional<llvm::StringRef>();
  }
}

llvm::Optional<llvm::StringRef> GenericVariableNamesCheck::nameIfVariableDecl(const MatchFinder::MatchResult &Result) {
  auto* MatchedDecl = Result.Nodes.getNodeAs<VarDecl>("var");
  if(MatchedDecl != nullptr) {
    return MatchedDecl->getName();
  } else {
    return llvm::Optional<llvm::StringRef>();
  }
}

void GenericVariableNamesCheck::check(const MatchFinder::MatchResult &Result) {
  // FIXME: Add callback implementation.
  auto MatchedDecl = Result.Nodes.getNodeAs<NamedDecl>("field");
  if(MatchedDecl == nullptr) {
      MatchedDecl = Result.Nodes.getNodeAs<NamedDecl>("var");
  }
  auto name = nameIfVariableDecl(Result).getValueOr(nameIfFieldDecl(Result).getValue());

  if (name == "data" || name == "m_data")
    diag(MatchedDecl->getLocation(), "Data too generic as a variable name, all variables hold data.")
      << MatchedDecl;
}

} // namespace voxel
} // namespace tidy
} // namespace clang
