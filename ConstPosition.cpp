#include "ConstPosition.h"
#include "llvm/ADT/StringSwitch.h"
#include "llvm/ADT/Optional.h"
namespace clang {
namespace tidy {
namespace voxel {

llvm::Optional<ConstPosition> ConstPositionFromString(llvm::StringRef value) {
  return llvm::StringSwitch<llvm::Optional<ConstPosition>>(value)
      .Case("Left", ConstPosition::Left)
      .Case("Right", ConstPosition::Right)
      .Default(llvm::Optional<ConstPosition>());
}

} // namespace voxel
} // namespace tidy
} // namespace clang
