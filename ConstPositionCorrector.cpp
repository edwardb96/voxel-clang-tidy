#include "ConstPositionCorrector.h"
#include "GetEndLoc.h"
#include "clang/Lex/Lexer.h"
#include <cassert>

namespace clang {
namespace tidy {
namespace voxel {

ConstPositionCorrectorBase::ConstPositionCorrectorBase(
    const ASTContext &Context)
    : Context(Context) {}

SourceLocation ConstPositionCorrector<ConstPosition::Left>::
operator()(const TypeLoc &TypeOccurence) const {
  return TypeOccurence.getBeginLoc();
}

SourceLocation ConstPositionCorrector<ConstPosition::Right>::
operator()(const TypeLoc &TypeOccurence) const {
  return getEndLoc(TypeOccurence);
}

SourceLocation ConstPositionCorrector<ConstPosition::Right>::getEndLoc(
    TypeLoc const &TypeOccurence) const {
  return EndCharLoc(Context, TypeOccurence);
}

} // namespace voxel
} // namespace tidy
} // namespace clang
