#include "../ClangTidy.h"
#include "../ClangTidyModule.h"
#include "../ClangTidyModuleRegistry.h"
#include "GenericVariableNamesCheck.h"
#include "ConstPositionCheck.h"
namespace clang {
namespace tidy {
namespace voxel {
class VoxelTidyModule : public ClangTidyModule {
public:
  void addCheckFactories(ClangTidyCheckFactories& CheckFactories) override {
    CheckFactories.registerCheck<GenericVariableNamesCheck>("voxel-generic-variable-name");
    CheckFactories.registerCheck<ConstPositionCheck>("voxel-const-position-check");
  }
};

static ClangTidyModuleRegistry::Add<VoxelTidyModule> X("voxel", "Adds voxel project-specific checks.");
} // namespace voxel

volatile int VoxelModuleAnchorSource = 0;
} // namespace tidy
} // namespace clang
