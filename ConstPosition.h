//===--- ConstPosition.h - clang-tidy----------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_CONST_POSITION_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_CONST_POSITION_H
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/StringRef.h"

namespace clang {
namespace tidy {
namespace voxel {
enum class ConstPosition { Left, Right };

constexpr ConstPosition Opposite(ConstPosition Position) {
  return (Position == ConstPosition::Left) ? ConstPosition::Right
                                           : ConstPosition::Left;
}

llvm::Optional<ConstPosition> ConstPositionFromString(llvm::StringRef value);

} // namespace voxel
} // namespace tidy
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_CONST_POSITION_H
