//===--- ConstPositionCheck.h - clang-tidy--------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_CONST_POSITION_CORRECTOR_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_CONST_POSITION_CORRECTOR_H

#include "ConstPosition.h"
#include "SimpleLexer.h"
#include "clang/AST/TypeLoc.h"
#include "clang/Basic/SourceLocation.h"

namespace clang {
namespace tidy {
namespace voxel {

class ConstPositionCorrectorBase {
public:
  ConstPositionCorrectorBase(const ASTContext &Context);
protected:
  const ASTContext &Context;
};

template <ConstPosition PreferredConstPosition> class ConstPositionCorrector;

template <>
class ConstPositionCorrector<ConstPosition::Left> : ConstPositionCorrectorBase {
public:
  using ConstPositionCorrectorBase::ConstPositionCorrectorBase;
  SourceLocation operator()(const TypeLoc &TypeOccurence) const;
};

template <>
class ConstPositionCorrector<ConstPosition::Right>
    : ConstPositionCorrectorBase {
public:
  using ConstPositionCorrectorBase::ConstPositionCorrectorBase;
  SourceLocation operator()(const TypeLoc &TypeOccurence) const;

private:
  SourceLocation getEndLoc(TypeLoc const &TypeOccurence) const;
};
} // namespace voxel
} // namespace tidy
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_CONST_POSITION_CORRECTOR_H
