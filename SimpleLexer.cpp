#include "SimpleLexer.h"

namespace clang {
namespace tidy {
namespace voxel {

LexResult::LexResult(NextAction WhatNext) : Tok(), WhatNext(WhatNext) {}
LexResult::LexResult(Token Tok) : Tok(Tok), WhatNext(NextAction::Abort) {}
bool LexResult::done() const { return WhatNext == NextAction::Abort; }
llvm::Optional<Token> LexResult::tokenElseNone() const { return Tok; }

LexResult Stop() { return LexResult(NextAction::Abort); }
LexResult Continue() { return LexResult(NextAction::Continue); }
LexResult Result(Token Tok) { return LexResult(Tok); }

SimpleLexer::SimpleLexer(SourceLocation StartHere, const ASTContext &Context)
    : RawLexerPtr(makeLexerStartingAt(Context.getSourceManager(),
                                      Context.getLangOpts(), StartHere)),
      RawLexer(*RawLexerPtr), Context(Context) {}

llvm::Optional<Token> SimpleLexer::nextToken() {
  Token Tok;
  if (!RawLexer.LexFromRawLexer(Tok)) {
    if (Tok.is(tok::raw_identifier))
      return withIdentifierInfo(Tok);
    else
      return Tok;
  } else {
    return llvm::None;
  }
}

Token SimpleLexer::withIdentifierInfo(Token Tok) const {
  auto &IdentifierInfo = getContext().Idents.get(identifierOf(Tok));
  Tok.setIdentifierInfo(&IdentifierInfo);
  Tok.setKind(IdentifierInfo.getTokenID());
  return Tok;
}

StringRef SimpleLexer::identifierOf(Token Tok) const {
  return identifierOfToken(getSources(), Tok);
}

StringRef SimpleLexer::identifierOfToken(const SourceManager &Sources,
                                         Token Tok) {
  return StringRef(Sources.getCharacterData(Tok.getLocation()),
                   Tok.getLength());
}

typename SimpleLexer::ClangLexer_uptr
SimpleLexer::makeLexerStartingAt(const SourceManager &Sources,
                                 const LangOptions &LangOpts,
                                 SourceLocation Start) {
  std::pair<FileID, unsigned> DecomposedLoc = Sources.getDecomposedLoc(Start);
  auto File = DecomposedLoc.first;
  auto OffsetInFile = DecomposedLoc.second;
  StringRef FileContent = Sources.getBufferData(File);
  const char *StartPoint = FileContent.data() + OffsetInFile;
  return llvm::make_unique<Lexer>(Sources.getLocForStartOfFile(File), LangOpts,
                                  FileContent.begin(), StartPoint,
                                  FileContent.end());
}

const ASTContext &SimpleLexer::getContext() const { return Context; }

const SourceManager &SimpleLexer::getSources() const {
  return Context.getSourceManager();
}

const LangOptions &SimpleLexer::getLangOpts() const {
  return Context.getLangOpts();
}

SimpleLexerFactory::SimpleLexerFactory(const ASTContext &Context)
    : Context(Context) {}

SimpleLexer SimpleLexerFactory::operator()(SourceLocation Loc) const {
  return SimpleLexer(Loc, Context);
}

bool IsBefore(const ASTContext &Context, SourceLocation First,
              SourceLocation Second) {
  return Context.getSourceManager().isBeforeInTranslationUnit(First, Second);
}

CharSourceRange FileCharRange(const ASTContext &Context,
                              CharSourceRange Range) {
  return Lexer::makeFileCharRange(Range, Context.getSourceManager(),
                                  Context.getLangOpts());
}

CharSourceRange RangeOfToken(const ASTContext &Context, Token Token) {
  return FileCharRange(Context, CharSourceRange::getCharRange(
                                    Token.getLocation(), Token.getEndLoc()));
}

CharSourceRange RangeExcludingStart(const ASTContext &Context,
                                    SourceLocation Start, SourceLocation End) {
  SourceLocation EndOfStart = Lexer::getLocForEndOfToken(
      Start, 0, Context.getSourceManager(), Context.getLangOpts());
  return FileCharRange(Context,
                       CharSourceRange::getTokenRange(EndOfStart, End));
}

CharSourceRange RangeExcludingEnd(const ASTContext &Context,
                                  SourceLocation Start, SourceLocation End) {
  SourceLocation StartOfEnd = End;
  return FileCharRange(Context,
                       CharSourceRange::getCharRange(Start, StartOfEnd));
}

} // namespace voxel
} // namespace tidy
} // namespace clang
