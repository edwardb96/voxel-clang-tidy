#include "TokenPredicates.h"
namespace clang {
namespace tidy {
namespace voxel {
// This stops us walking out of a template
// argument. FIXME: Test these.
// X<int* const, int* const>
//               ^
// const X<int const* const>
//
bool IsRightTemplateArgBoundary(Token Tok) {
  return Tok.isOneOf(tok::comma, tok::greater);
}

bool IsLeftTemplateArgBoundary(Token Tok) {
  return Tok.isOneOf(tok::comma, tok::less);
}

bool IsLeftTypeEdge(Token Tok) { return IsLeftTemplateArgBoundary(Tok); }

bool IsRightTypeEdge(Token Tok) {
  return Tok.isOneOf(tok::star, tok::amp) || IsLeftTemplateArgBoundary(Tok);
}

bool IsLeftReturnTypeEdge(Token Tok) {
  return IsLeftTypeEdge(Tok) || Tok.is(tok::arrow);
}

bool IsRightReturnTypeEdge(Token Tok) {
  return IsRightTypeEdge(Tok) || Tok.isOneOf(tok::l_brace, tok::identifier);
}

bool IsConst(Token Tok) { return Tok.is(tok::kw_const); }


}
} // namespace tidy
} // namespace clang
