#include "ReturnTypeNodes.h"
namespace clang {
namespace ast_matchers {
internal::Matcher<Decl> functionDeclReturnTypeContainingConst() {
  return functionDecl(
             returnTypeLoc(anyOf(isConstNonPointerLoc(), findConstRefereeLocs(),
                                 findAllNonPointerConstPointeeLocs())))
      .bind("decl");
}

internal::Matcher<Stmt> lambdaExprReturnTypeContainingConst() {
  return lambdaExpr(hasCallOperator(functionDecl(returnTypeLoc(
                        anyOf(isConstNonPointerLoc(), findConstRefereeLocs(),
                              findAllNonPointerConstPointeeLocs())))))
      .bind("expr");
}
} // namespace ast_matchers
namespace tidy {
namespace voxel {
llvm::Optional<FunctionReturnTypeNodes> ExtractFunctionReturnTypeNodes::
operator()(const ast_matchers::BoundNodes &BoundNodes) const {
  return MakeIfAllHaveValues<FunctionReturnTypeNodes>(
      Extract<FunctionDecl>(BoundNodes, "decl"),
      Extract<TypeLoc>(BoundNodes, "loc"));
}

llvm::Optional<LambdaReturnTypeNodes> ExtractLambdaReturnTypeNodes::
operator()(const ast_matchers::BoundNodes &BoundNodes) const {
  return MakeIfAllHaveValues<LambdaReturnTypeNodes>(
      Extract<LambdaExpr>(BoundNodes, "expr"),
      Extract<TypeLoc>(BoundNodes, "loc"));
}
} // namespace voxel
} // namespace tidy
} // namespace clang
