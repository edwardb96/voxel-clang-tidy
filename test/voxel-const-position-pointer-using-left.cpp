// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++11

// CHECK-MESSAGES: :[[@LINE+1]]:15: warning: misplaced const. [voxel-const-position-check]
using A = int const *const;
// CHECK-FIXES: {{^}}using A = const int *const;{{$}}

// CHECK-MESSAGES: :[[@LINE+1]]:15: warning: misplaced const. [voxel-const-position-check]
using B = int const *const *const;
// CHECK-FIXES: {{^}}using B = const int *const *const;{{$}}

template <bool>
// CHECK-MESSAGES: :[[@LINE+1]]:15: warning: misplaced const. [voxel-const-position-check]
using C = int const *const *const;
// CHECK-FIXES: {{^}}template <bool>{{$}}
// CHECK-FIXES: {{^}}using C = const int *const *const;{{$}}

using D = int*;

// CHECK-MESSAGES: :[[@LINE+1]]:13: warning: misplaced const. [voxel-const-position-check]
using E = D const;
// CHECK-FIXES: {{^}}using E = const D ;{{$}}

using F = const int*;

// CHECK-MESSAGES: :[[@LINE+1]]:13: warning: misplaced const. [voxel-const-position-check]
using G = F const;
// CHECK-FIXES: {{^}}using G = const F ;{{$}}
