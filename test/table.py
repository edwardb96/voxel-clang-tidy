#!/usr/bin/env python3

contexts = [
    "var",
    "field",
    "function-return-type",
    "lambda-return-type",
    "using",
    "class-template",
    "function-template",
    "typedef"]
subcontexts = ["pointer", "primative", "reference", "array"]
positions = ["left", "right"]

tbl = [[[(pos, cont, sub) for pos in positions] for cont in contexts] for sub in subcontexts]

maxcont = max(len(cont) for cont in contexts)
maxsubcont = max(len(sub) for sub in subcontexts)
maxposition = max(len(pos) for pos in positions)

for sublst in tbl:
    for contlst in sublst:
        for pos, cont, sub in contlst:
            print("{} {} {}".format(sub.ljust(maxsubcont), cont.ljust(maxcont), pos.ljust(maxposition)))
