// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++11

int target = 10;

class Main {
  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const *const a = nullptr;
  // CHECK-FIXES: {{^}}  const int *const a = nullptr;{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const *const b = &target;
  // CHECK-FIXES: {{^}}  const int *const b = &target;{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const *const c{nullptr};
  // CHECK-FIXES: {{^}}  const int *const c{nullptr};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const *const *const d{&c};
  // CHECK-FIXES: {{^}}  const int *const *const d{&c};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const *m_ptr;
  // CHECK-FIXES: {{^}}  const int *m_ptr;{{$}}
};
