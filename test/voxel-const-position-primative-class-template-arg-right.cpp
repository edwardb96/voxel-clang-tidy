// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++11

template <typename T>
class MyClassAlpha {};

template <typename T1, typename T2>
class MyClassBeta {};

template <typename T1, typename T2, typename T3>
class MyClassGamma {};

template <typename... Ts>
class MyClassTheta {};

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:29: warning: misplaced const. [voxel-const-position-check]
  auto Alpha = MyClassAlpha<const int>();
  // CHECK-FIXES: {{^}}  auto Alpha = MyClassAlpha<int const>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+2]]:27: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:38: warning: misplaced const. [voxel-const-position-check]
  auto Beta = MyClassBeta<const int, const char>();
  // CHECK-FIXES: {{^}}  auto Beta = MyClassBeta<int const, char const>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+3]]:29: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:40: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:52: warning: misplaced const. [voxel-const-position-check]
  auto Gamma = MyClassGamma<const int, const char, const unsigned long>();
  // CHECK-FIXES: {{^}}  auto Gamma = MyClassGamma<int const, char const, unsigned long const>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+4]]:29: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+3]]:40: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:52: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:73: warning: misplaced const. [voxel-const-position-check]
  auto Theta = MyClassTheta<const int, const char, const unsigned long, const short>();
  // CHECK-FIXES: {{^}}  auto Theta = MyClassTheta<int const, char const, unsigned long const, short const>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+3]]:35: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:54: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:73: warning: misplaced const. [voxel-const-position-check]
  auto NestedAlpha = MyClassAlpha<const MyClassAlpha<const MyClassAlpha<const int>>>();
  // CHECK-FIXES: {{^}}  auto NestedAlpha = MyClassAlpha<MyClassAlpha<MyClassAlpha<int const> const> const>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+6]]:33: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+5]]:51: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+4]]:69: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+3]]:80: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:101: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:114: warning: misplaced const. [voxel-const-position-check]
  auto NestedBeta = MyClassBeta<const MyClassBeta<const MyClassBeta<const int, const unsigned int>, const bool>, const int>();
  // CHECK-FIXES: {{^}}  auto NestedBeta = MyClassBeta<MyClassBeta<MyClassBeta<int const, unsigned int const> const, bool const> const, int const>();

  return 0;
}
