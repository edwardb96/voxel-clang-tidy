// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++14

int main() {
  auto refereeChar = '+';
  auto refereeInt = 1;

  // CHECK-MESSAGES: :[[@LINE+1]]:30: warning: misplaced const. [voxel-const-position-check]
  auto a = [&](int Y) -> int const & {
    // CHECK-FIXES: {{^}}  auto a = [&](int Y) -> const int & {{{$}}
    return refereeInt;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:54: warning: misplaced const. [voxel-const-position-check]
  auto b = [&a, refereeChar](int Y) -> unsigned char const & {
    // CHECK-FIXES: {{^}}  auto b = [&a, refereeChar](int Y) -> const unsigned char & {{{$}}
    return refereeChar;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:55: warning: misplaced const. [voxel-const-position-check]
  auto c = [a, b, refereeChar](int Y) mutable -> char const & {
    // CHECK-FIXES: {{^}}  auto c = [a, b, refereeChar](int Y) mutable -> const char & {{{$}}
    return refereeChar;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:34: warning: misplaced const. [voxel-const-position-check]
  auto d = [refereeInt]() -> int const & {
    // CHECK-FIXES: {{^}}  auto d = [refereeInt]() -> const int & {{{$}}
    return refereeInt;
  };
}
