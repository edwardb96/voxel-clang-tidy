// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++11

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int a[] = {1, 2, 3, 4};
  // CHECK-FIXES: {{^}}  int const a[] = {1, 2, 3, 4};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const char b[] = {'z', 'x', 'y'};
  // CHECK-FIXES: {{^}}  char const b[] = {'z', 'x', 'y'};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int c[]{7, 9, 2, 1};
  // CHECK-FIXES: {{^}}  int const c[]{7, 9, 2, 1};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int e[5] = {0, 0, 0, 0, 0};
  // CHECK-FIXES: {{^}}  int const e[5] = {0, 0, 0, 0, 0};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const float f[2] = {1.0f, 2.5f};
  // CHECK-FIXES: {{^}}  float const f[2] = {1.0f, 2.5f};{{$}}

  return 0;
}
