// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++11

class Main {
  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const a = 5;
  // CHECK-FIXES: {{^}}  const int a = 5;{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:16: warning: misplaced const. [voxel-const-position-check]
  static float const b;
  // CHECK-FIXES: {{^}}  static const float b;{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const c{7};
  // CHECK-FIXES: {{^}}  const int c{7};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:9: warning: misplaced const. [voxel-const-position-check]
  float const d{-7.8f};
  // CHECK-FIXES: {{^}}  const float d{-7.8f};{{$}}
};

// CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
float const Main::b = 5.0f;
// CHECK-FIXES: {{^}}const float Main::b = 5.0f;{{$}}
