// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++11

template <typename T>
class MyClassAlpha {};

template <typename T1, typename T2>
class MyClassBeta {};

template <typename T1, typename T2, typename T3>
class MyClassGamma {};

template <typename... Ts>
class MyClassTheta {};

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:33: warning: misplaced const. [voxel-const-position-check]
  auto Alpha = MyClassAlpha<int const *>();
  // CHECK-FIXES: {{^}}  auto Alpha = MyClassAlpha<const int *>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+2]]:32: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:43: warning: misplaced const. [voxel-const-position-check]
  auto Beta = MyClassBeta<char const, int const *>();
  // CHECK-FIXES: {{^}}  auto Beta = MyClassBeta<const char , const int *>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+3]]:33: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:52: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:80: warning: misplaced const. [voxel-const-position-check]
  auto Gamma = MyClassGamma<int const *const, char const *const, unsigned long const *>();
  // CHECK-FIXES: {{^}}  auto Gamma = MyClassGamma<const int *const, const char *const, const unsigned long *>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+4]]:33: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+3]]:52: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:87: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:108: warning: misplaced const. [voxel-const-position-check]
  auto Theta = MyClassTheta<int const *const, char const *const *const, unsigned long const **const, short const *const>();
  // CHECK-FIXES: {{^}}  auto Theta = MyClassTheta<const int *const, const char *const *const, const unsigned long **const, const short *const>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+3]]:65: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:87: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:94: warning: misplaced const. [voxel-const-position-check]
  auto NestedAlpha = MyClassAlpha<MyClassAlpha<MyClassAlpha<int const **const *const> const> const>();
  // CHECK-FIXES: {{^}}  auto NestedAlpha = MyClassAlpha<const MyClassAlpha<const MyClassAlpha<const int **const *const> > >();{{$}}

  // CHECK-MESSAGES: :[[@LINE+5]]:61: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+4]]:83: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+3]]:92: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:104: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:132: warning: misplaced const. [voxel-const-position-check]
  auto NestedBeta = MyClassBeta<MyClassBeta<MyClassBeta<int const *, unsigned int const *> const, bool const *const *const *const> const *const, int **>();
  // CHECK-FIXES: {{^}}  auto NestedBeta = MyClassBeta<const MyClassBeta<const MyClassBeta<const int *, const unsigned int *> , const bool *const *const *const> *const, int **>();

  return 0;
}
