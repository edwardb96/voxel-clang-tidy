// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++11

// CHECK-MESSAGES: :[[@LINE+1]]:11: warning: misplaced const. [voxel-const-position-check]
using A = const int *const;
// CHECK-FIXES: {{^}}using A = int const *const;{{$}}

// CHECK-MESSAGES: :[[@LINE+1]]:11: warning: misplaced const. [voxel-const-position-check]
using B = const int *const *const;
// CHECK-FIXES: {{^}}using B = int const *const *const;{{$}}

template <bool>
// CHECK-MESSAGES: :[[@LINE+1]]:11: warning: misplaced const. [voxel-const-position-check]
using C = const int *const *const;
// CHECK-FIXES: {{^}}template <bool>{{$}}
// CHECK-NEXT: {{^}}using C = int const *const *const;{{$}}

using D = int*;

// CHECK-MESSAGES: :[[@LINE+1]]:11: warning: misplaced const. [voxel-const-position-check]
using E = const D;
// CHECK-FIXES: {{^}}using E = D const;{{$}}

using F = int const*;

// CHECK-MESSAGES: :[[@LINE+1]]:11: warning: misplaced const. [voxel-const-position-check]
using G = const F;
// CHECK-FIXES: {{^}}using G = F const;{{$}}
