// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++14

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:29: warning: misplaced const. [voxel-const-position-check]
  auto a = [](int Y) -> int const {
    // CHECK-FIXES: {{^}}  auto a = [](int Y) -> const int {{{$}}
    return 0;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:41: warning: misplaced const. [voxel-const-position-check]
  auto b = [&a](int Y) -> unsigned char const {
    // CHECK-FIXES: {{^}}  auto b = [&a](int Y) -> const unsigned char {{{$}}
    return 'a';
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:42: warning: misplaced const. [voxel-const-position-check]
  auto c = [a, b](int Y) mutable -> char const {
    // CHECK-FIXES: {{^}}  auto c = [a, b](int Y) mutable -> const char {{{$}}
    return 'b';
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:24: warning: misplaced const. [voxel-const-position-check]
  auto d = []() -> int const {
    // CHECK-FIXES: {{^}}  auto d = []() -> const int {{{$}}
    return 1;
  };
}
