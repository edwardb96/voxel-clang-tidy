// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++11

class Main {
  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int &a = 5;
  // CHECK-FIXES: {{^}}  int const &a = 5;{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:10: warning: misplaced const. [voxel-const-position-check]
  static const float &b;
  // CHECK-FIXES: {{^}}  static float const &b;{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int &c{7};
  // CHECK-FIXES: {{^}}  int const &c{7};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const float &d{-7.8f};
  // CHECK-FIXES: {{^}}  float const &d{-7.8f};{{$}}
};

// CHECK-MESSAGES: :[[@LINE+1]]:1: warning: misplaced const. [voxel-const-position-check]
const float &Main::b = 5.0f;
// CHECK-FIXES: {{^}}float const &Main::b = 5.0f;{{$}}
