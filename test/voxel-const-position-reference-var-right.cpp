// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++11

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const unsigned int &a = 5;
  // CHECK-FIXES: {{^}}  unsigned int const &a = 5;{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const auto &b = 14.5f;
  // CHECK-FIXES: {{^}}  auto const &b = 14.5f;{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const auto &c{7};
  // CHECK-FIXES: {{^}}  auto const &c{7};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int &d(11);
  // CHECK-FIXES: {{^}}  int const &d(11);{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const auto &e(-6.8f);
  // CHECK-FIXES: {{^}}  auto const &e(-6.8f);{{$}}

  return 0;
}
