// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++11

template <typename T>
void alpha() {}

template <typename T1, typename T2>
void beta(){};

template <typename T1, typename T2, typename T3>
void gamma(){};

template <typename... Ts>
void theta(){};

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:9: warning: misplaced const. [voxel-const-position-check]
  alpha<const int &>();
  // CHECK-FIXES: {{^}}  alpha<int const &>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+2]]:8: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:21: warning: misplaced const. [voxel-const-position-check]
  beta<const int &, const char &>();
  // CHECK-FIXES: {{^}}  beta<int const &, char const &>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+3]]:9: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:22: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:36: warning: misplaced const. [voxel-const-position-check]
  gamma<const int &, const char &, const unsigned long &>();
  // CHECK-FIXES: {{^}}  gamma<int const &, char const &, unsigned long const &>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+4]]:9: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+3]]:22: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:36: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:59: warning: misplaced const. [voxel-const-position-check]
  theta<const int &, const char &, const unsigned long &, const short &>();
  // CHECK-FIXES: {{^}}  theta<int const &, char const &, unsigned long const &, short const &>();{{$}}

  return 0;
}
