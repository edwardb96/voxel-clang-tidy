// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++14
int refereeInt = 0;
char refereeChar = '-';

class X {
  X(int Y) {
  }

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int &a(int Y) {
    // CHECK-FIXES: {{^}}  int const &a(int Y) {{{$}}
    return refereeInt;
  }

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const char &b(int Y) const {
    // CHECK-FIXES: {{^}}  char const &b(int Y) const {{{$}}
    return refereeChar;
  }

  auto notThis(int Y) const -> int const & {
    return refereeInt;
  }

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int &c(int Y) const;
  // CHECK-FIXES: {{^}}  int const &c(int Y) const;{{$}}
};

// CHECK-MESSAGES: :[[@LINE+1]]:1: warning: misplaced const. [voxel-const-position-check]
const int &X::c(int Y) const {
  // CHECK-FIXES: {{^}}int const &X::c(int Y) const {{{$}}
  return refereeInt;
}

// CHECK-MESSAGES: :[[@LINE+1]]:1: warning: misplaced const. [voxel-const-position-check]
const auto &d(char Y) {
  // CHECK-FIXES: {{^}}auto const &d(char Y) {{{$}}
  return refereeChar;
}

// CHECK-MESSAGES: :[[@LINE+1]]:26: warning: misplaced const. [voxel-const-position-check]
auto e(char const *Y) -> const char & {
  // CHECK-FIXES: {{^}}auto e(char const *Y) -> char const & {{{$}}
  return refereeChar;
}

// CHECK-MESSAGES: :[[@LINE+1]]:1: warning: misplaced const. [voxel-const-position-check]
const int &f() {
  // CHECK-FIXES: {{^}}int const &f() {{{$}}
  return refereeInt;
}
