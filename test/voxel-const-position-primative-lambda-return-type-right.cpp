// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++14

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:25: warning: misplaced const. [voxel-const-position-check]
  auto a = [](int Y) -> const int {
    // CHECK-FIXES: {{^}}  auto a = [](int Y) -> int const {{{$}}
    return 0;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:27: warning: misplaced const. [voxel-const-position-check]
  auto b = [&a](int Y) -> const unsigned char {
    // CHECK-FIXES: {{^}}  auto b = [&a](int Y) -> unsigned char const {{{$}}
    return 'a';
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:37: warning: misplaced const. [voxel-const-position-check]
  auto c = [a, b](int Y) mutable -> const char {
    // CHECK-FIXES: {{^}}  auto c = [a, b](int Y) mutable -> char const {{{$}}
    return 'b';
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:20: warning: misplaced const. [voxel-const-position-check]
  auto d = []() -> const int {
    // CHECK-FIXES: {{^}}  auto d = []() -> int const {{{$}}
    return 1;
  };
}
