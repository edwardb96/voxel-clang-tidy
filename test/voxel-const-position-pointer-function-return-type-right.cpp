// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++14

int pointee = 10;

class X {
  X(int Y) {
  }

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int *const a(int Y) {
    // CHECK-FIXES: {{^}}  int const *const a(int Y) {{{$}}
    return nullptr;
  }

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const char *const b(int Y) const {
    // CHECK-FIXES: {{^}}  char const *const b(int Y) const {{{$}}
    return nullptr;
  }

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const char *const c(int Y) const;
  // CHECK-FIXES: {{^}}  char const *const c(int Y) const;{{$}}
};

// CHECK-MESSAGES: :[[@LINE+1]]:1: warning: misplaced const. [voxel-const-position-check]
const char *const X::c(int Y) const {
  // CHECK-FIXES: {{^}}char const *const X::c(int Y) const {{{$}}
  return nullptr;
}

// CHECK-MESSAGES: :[[@LINE+1]]:1: warning: misplaced const. [voxel-const-position-check]
const auto *const d(char Y) {
  // CHECK-FIXES: {{^}}auto const *const d(char Y) {{{$}}
  return &pointee;
}

// CHECK-MESSAGES: :[[@LINE+1]]:26: warning: misplaced const. [voxel-const-position-check]
auto e(char const *Y) -> const int *const {
  // CHECK-FIXES: {{^}}auto e(char const *Y) -> int const *const {{{$}}
  return nullptr;
}

// CHECK-MESSAGES: :[[@LINE+1]]:13: warning: misplaced const. [voxel-const-position-check]
auto f() -> const int *const {
  // CHECK-FIXES: {{^}}auto f() -> int const *const {{{$}}
  return nullptr;
}

// CHECK-MESSAGES: :[[@LINE+1]]:1: warning: misplaced const. [voxel-const-position-check]
const int *const *const g() {
  // CHECK-FIXES: {{^}}int const *const *const g() {{{$}}
  return nullptr;
}

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:20: warning: misplaced const. [voxel-const-position-check]
  auto h = []() -> const int *const {
    // CHECK-FIXES: {{^}}  auto h = []() -> int const *const {{{$}}
    return nullptr;
  };
}
