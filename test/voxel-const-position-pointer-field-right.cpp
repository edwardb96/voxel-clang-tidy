// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++11

int target = 10;

class Main {
  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int *const a = nullptr;
  // CHECK-FIXES: {{^}}  int const *const a = nullptr;{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int *const b = &target;
  // CHECK-FIXES: {{^}}  int const *const b = &target;{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int *const c{nullptr};
  // CHECK-FIXES: {{^}}  int const *const c{nullptr};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int *const *const d{&c};
  // CHECK-FIXES: {{^}}  int const *const *const d{&c};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int *m_ptr;
  // CHECK-FIXES: {{^}}  int const *m_ptr;{{$}}
};
