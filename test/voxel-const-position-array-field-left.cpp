// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++11

int target = 10;

class Alpha {
  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const a[];
  // CHECK-FIXES: {{^}}  const int a[];{{$}}
};

class Beta {
  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const b[2] = {1, 2};
  // CHECK-FIXES: {{^}}  const int b[2] = {1, 2};{{$}}
};

class Gamma {
  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const *c[1] = {nullptr};
  // CHECK-FIXES: {{^}}  const int *c[1] = {nullptr};{{$}}
};

class Theta {
  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const d[3][3] = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};
  // CHECK-FIXES: {{^}}  const int d[3][3] = {{[{][{]}}0, 1, 2}, {3, 4, 5}, {6, 7, 8}};{{$}}
};

class Zeta {
  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const e[5] = {1, 2, 3, 4, 5};
  // CHECK-FIXES: {{^}}  const int e[5] = {1, 2, 3, 4, 5};{{$}}
};
