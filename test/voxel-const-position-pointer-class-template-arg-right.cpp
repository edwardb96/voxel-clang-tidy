// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++11

template <typename T>
class MyClassAlpha {};

template <typename T1, typename T2>
class MyClassBeta {};

template <typename T1, typename T2, typename T3>
class MyClassGamma {};

template <typename... Ts>
class MyClassTheta {};

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:29: warning: misplaced const. [voxel-const-position-check]
  auto Alpha = MyClassAlpha<const int *>();
  // CHECK-FIXES: {{^}}  auto Alpha = MyClassAlpha<int const *>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+2]]:27: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:39: warning: misplaced const. [voxel-const-position-check]
  auto Beta = MyClassBeta<const char, const int *>();
  // CHECK-FIXES: {{^}}  auto Beta = MyClassBeta<char const, int const *>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+3]]:29: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:47: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:66: warning: misplaced const. [voxel-const-position-check]
  auto Gamma = MyClassGamma<const int *const, const char *const, const unsigned long *>();
  // CHECK-FIXES: {{^}}  auto Gamma = MyClassGamma<int const *const, char const *const, unsigned long const *>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+4]]:29: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+3]]:47: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:73: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:102: warning: misplaced const. [voxel-const-position-check]
  auto Theta = MyClassTheta<const int *const, const char *const *const, const unsigned long **const, const short *const>();
  // CHECK-FIXES: {{^}}  auto Theta = MyClassTheta<int const *const, char const *const *const, unsigned long const **const, short const *const>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+3]]:35: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:54: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:73: warning: misplaced const. [voxel-const-position-check]
  auto NestedAlpha = MyClassAlpha<const MyClassAlpha<const MyClassAlpha<const int **const *const>>>();
  // CHECK-FIXES: {{^}}  auto NestedAlpha = MyClassAlpha<MyClassAlpha<MyClassAlpha<int const **const *const> const> const>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+5]]:33: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+4]]:51: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+3]]:69: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:82: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:105: warning: misplaced const. [voxel-const-position-check]
  auto NestedBeta = MyClassBeta<const MyClassBeta<const MyClassBeta<const int *, const unsigned int *>, const bool *const *const *const> *const, int **>();
  // CHECK-FIXES: {{^}}  auto NestedBeta = MyClassBeta<MyClassBeta<MyClassBeta<int const *, unsigned int const *> const, bool const *const *const *const> const *const, int **>();

  return 0;
}
