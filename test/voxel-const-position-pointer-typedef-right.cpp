// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++11

// CHECK-MESSAGES: :[[@LINE+1]]:9: warning: misplaced const. [voxel-const-position-check]
typedef const int *const A;
// CHECK-FIXES: {{^}}typedef int const *const A;{{$}}

// CHECK-MESSAGES: :[[@LINE+1]]:9: warning: misplaced const. [voxel-const-position-check]
typedef const int *const *const B;
// CHECK-FIXES: {{^}}typedef int const *const *const B;{{$}}

typedef int* D;

// CHECK-MESSAGES: :[[@LINE+1]]:9: warning: misplaced const. [voxel-const-position-check]
typedef const D E;
// CHECK-FIXES: {{^}}typedef D const E;{{$}}

typedef int const* F;

// CHECK-MESSAGES: :[[@LINE+1]]:9: warning: misplaced const. [voxel-const-position-check]
typedef const F G;
// CHECK-FIXES: {{^}}typedef F const G;{{$}}
