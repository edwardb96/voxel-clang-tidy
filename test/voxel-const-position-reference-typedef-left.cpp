// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++11

// CHECK-MESSAGES: :[[@LINE+1]]:13: warning: misplaced const. [voxel-const-position-check]
typedef int const &Alpha;
// CHECK-FIXES: {{^}}typedef const int &Alpha;{{$}}

using Gamma = unsigned;

// CHECK-MESSAGES: :[[@LINE+1]]:15: warning: misplaced const. [voxel-const-position-check]
typedef Gamma const &Delta;
// CHECK-FIXES: {{^}}typedef const Gamma &Delta;{{$}}
