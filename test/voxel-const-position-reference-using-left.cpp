// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++11

// CHECK-MESSAGES: :[[@LINE+1]]:19: warning: misplaced const. [voxel-const-position-check]
using Alpha = int const &;
// CHECK-FIXES: {{^}}using Alpha = const int &;{{$}}

template <int>
// CHECK-MESSAGES: :[[@LINE+1]]:19: warning: misplaced const. [voxel-const-position-check]
using Beta = char const &;
// CHECK-FIXES: {{^}}template <int>{{$}}
// CHECK-NEXT: {{^}}using Beta = const char &;{{$}}

using Gamma = unsigned;

// CHECK-MESSAGES: :[[@LINE+1]]:21: warning: misplaced const. [voxel-const-position-check]
using Delta = Gamma const &;
// CHECK-FIXES: {{^}}using Delta = const Gamma &;{{$}}
