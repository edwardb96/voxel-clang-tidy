// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++11

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const a = 5;
  // CHECK-FIXES: {{^}}  const int a = 5;{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:8: warning: misplaced const. [voxel-const-position-check]
  auto const b = 14.5f;
  // CHECK-FIXES: {{^}}  const auto b = 14.5f;{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const c{7};
  // CHECK-FIXES: {{^}}  const int c{7};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:8: warning: misplaced const. [voxel-const-position-check]
  auto const d{7};
  // CHECK-FIXES: {{^}}  const auto d{7};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const e(11);
  // CHECK-FIXES: {{^}}  const int e(11);{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:8: warning: misplaced const. [voxel-const-position-check]
  auto const f(-6.8f);
  // CHECK-FIXES: {{^}}  const auto f(-6.8f);{{$}}

  return 0;
}
