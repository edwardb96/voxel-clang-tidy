// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++11

template <typename T>
class MyClassAlpha {};

template <typename T1, typename T2>
class MyClassBeta {};

template <typename T1, typename T2, typename T3>
class MyClassGamma {};

template <typename... Ts>
class MyClassTheta {};

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:33: warning: misplaced const. [voxel-const-position-check]
  auto Alpha = MyClassAlpha<int const>();
  // CHECK-FIXES: {{^}}  auto Alpha = MyClassAlpha<const int >();{{$}}

  // CHECK-MESSAGES: :[[@LINE+2]]:31: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:43: warning: misplaced const. [voxel-const-position-check]
  auto Beta = MyClassBeta<int const, char const>();
  // CHECK-FIXES: {{^}}  auto Beta = MyClassBeta<const int , const char >();{{$}}

  // CHECK-MESSAGES: :[[@LINE+3]]:33: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:45: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:66: warning: misplaced const. [voxel-const-position-check]
  auto Gamma = MyClassGamma<int const, char const, unsigned long const>();
  // CHECK-FIXES: {{^}}  auto Gamma = MyClassGamma<const int , const char , const unsigned long >();{{$}}

  // CHECK-MESSAGES: :[[@LINE+4]]:33: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+3]]:45: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:66: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:79: warning: misplaced const. [voxel-const-position-check]
  auto Theta = MyClassTheta<int const, char const, unsigned long const, short const>();
  // CHECK-FIXES: {{^}}  auto Theta = MyClassTheta<const int , const char , const unsigned long , const short >();{{$}}

  // CHECK-MESSAGES: :[[@LINE+3]]:65: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:72: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:79: warning: misplaced const. [voxel-const-position-check]
  auto NestedAlpha = MyClassAlpha<MyClassAlpha<MyClassAlpha<int const> const> const>();
  // CHECK-FIXES: {{^}}  auto NestedAlpha = MyClassAlpha<const MyClassAlpha<const MyClassAlpha<const int > > >();{{$}}

  // CHECK-MESSAGES: :[[@LINE+6]]:61: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+5]]:81: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+4]]:88: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+3]]:100: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:107: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:118: warning: misplaced const. [voxel-const-position-check]
  auto NestedBeta = MyClassBeta<MyClassBeta<MyClassBeta<int const, unsigned int const> const, bool const> const, int const>();
  // CHECK-FIXES: {{^}}  auto NestedBeta = MyClassBeta<const MyClassBeta<const MyClassBeta<const int , const unsigned int > , const bool > , const int >();

  return 0;
}
