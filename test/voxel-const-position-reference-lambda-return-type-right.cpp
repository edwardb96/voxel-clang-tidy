// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++14

int main() {
  auto refereeChar = '+';
  auto refereeInt = 1;

  // CHECK-MESSAGES: :[[@LINE+1]]:26: warning: misplaced const. [voxel-const-position-check]
  auto a = [&](int Y) -> const int & {
    // CHECK-FIXES: {{^}}  auto a = [&](int Y) -> int const & {{{$}}
    return refereeInt;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:40: warning: misplaced const. [voxel-const-position-check]
  auto b = [&a, refereeChar](int Y) -> const unsigned char & {
    // CHECK-FIXES: {{^}}  auto b = [&a, refereeChar](int Y) -> unsigned char const & {{{$}}
    return refereeChar;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:50: warning: misplaced const. [voxel-const-position-check]
  auto c = [a, b, refereeChar](int Y) mutable -> const char & {
    // CHECK-FIXES: {{^}}  auto c = [a, b, refereeChar](int Y) mutable -> char const & {{{$}}
    return refereeChar;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:30: warning: misplaced const. [voxel-const-position-check]
  auto d = [refereeInt]() -> const int & {
    // CHECK-FIXES: {{^}}  auto d = [refereeInt]() -> int const & {{{$}}
    return refereeInt;
  };
}
