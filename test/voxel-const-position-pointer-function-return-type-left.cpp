// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++14

int pointee = 10;

class X {
  X(int Y) {
  }

  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const *const a(int Y) {
    // CHECK-FIXES: {{^}}  const int *const a(int Y) {{{$}}
    return nullptr;
  }

  // CHECK-MESSAGES: :[[@LINE+1]]:8: warning: misplaced const. [voxel-const-position-check]
  char const *const b(int Y) const {
    // CHECK-FIXES: {{^}}  const char *const b(int Y) const {{{$}}
    return nullptr;
  }

  // CHECK-MESSAGES: :[[@LINE+1]]:8: warning: misplaced const. [voxel-const-position-check]
  char const *const c(int Y) const;
  // CHECK-FIXES: {{^}}  const char *const c(int Y) const;{{$}}
};

// CHECK-MESSAGES: :[[@LINE+1]]:6: warning: misplaced const. [voxel-const-position-check]
char const *const X::c(int Y) const {
  // CHECK-FIXES: {{^}}const char *const X::c(int Y) const {{{$}}
  return nullptr;
}

// CHECK-MESSAGES: :[[@LINE+1]]:6: warning: misplaced const. [voxel-const-position-check]
auto const *const d(char Y) {
  // CHECK-FIXES: {{^}}const auto *const d(char Y) {{{$}}
  return &pointee;
}

// CHECK-MESSAGES: :[[@LINE+1]]:30: warning: misplaced const. [voxel-const-position-check]
auto e(const char *Y) -> int const *const {
  // CHECK-FIXES: {{^}}auto e(const char *Y) -> const int *const {{{$}}
  return nullptr;
}

// CHECK-MESSAGES: :[[@LINE+1]]:17: warning: misplaced const. [voxel-const-position-check]
auto f() -> int const *const {
  // CHECK-FIXES: {{^}}auto f() -> const int *const {{{$}}
  return nullptr;
}

// CHECK-MESSAGES: :[[@LINE+1]]:5: warning: misplaced const. [voxel-const-position-check]
int const *const *const g() {
  // CHECK-FIXES: {{^}}const int *const *const g() {{{$}}
  return nullptr;
}

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:24: warning: misplaced const. [voxel-const-position-check]
  auto h = []() -> int const* const {
    // CHECK-FIXES: {{^}}  auto h = []() -> const int * const {{{$}}
    return nullptr;
  };
}
