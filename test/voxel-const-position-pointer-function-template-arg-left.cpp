// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++11

template <typename T>
void alpha(){};

template <typename T1, typename T2>
void beta(){};

template <typename T1, typename T2, typename T3>
void gamma(){};

template <typename... Ts>
void theta(){};

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:13: warning: misplaced const. [voxel-const-position-check]
  alpha<int const *>();
  // CHECK-FIXES: {{^}}  alpha<const int *>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+2]]:13: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:24: warning: misplaced const. [voxel-const-position-check]
  beta<char const, int const *>();
  // CHECK-FIXES: {{^}}  beta<const char , const int *>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+3]]:13: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:32: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:60: warning: misplaced const. [voxel-const-position-check]
  gamma<int const *const, char const *const, unsigned long const *>();
  // CHECK-FIXES: {{^}}  gamma<const int *const, const char *const, const unsigned long *>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+4]]:13: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+3]]:39: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:74: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:95: warning: misplaced const. [voxel-const-position-check]
  theta<int const *const *const, char const *const *const, unsigned long const **const, short const *const>();
  // CHECK-FIXES: {{^}}  theta<const int *const *const, const char *const *const, const unsigned long **const, const short *const>();{{$}}

  return 0;
}
