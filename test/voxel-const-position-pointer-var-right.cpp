// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++11

int target = 10;

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int *const a = nullptr;
  // CHECK-FIXES: {{^}}  int const *const a = nullptr;{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const auto *const b = &target;
  // CHECK-FIXES: {{^}}  auto const *const b = &target;{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int *const e(&target);
  // CHECK-FIXES: {{^}}  int const *const e(&target);{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int *const *const f(&e);
  // CHECK-FIXES: {{^}}  int const *const *const f(&e);{{$}}

  return 0;
}
