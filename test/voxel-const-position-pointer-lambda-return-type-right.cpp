// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++14

int main() {
  int pointee = 10;

  // CHECK-MESSAGES: :[[@LINE+1]]:25: warning: misplaced const. [voxel-const-position-check]
  auto a = [](int Y) -> const int *const {
    // CHECK-FIXES: {{^}}  auto a = [](int Y) -> int const *const {{{$}}
    return nullptr;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:27: warning: misplaced const. [voxel-const-position-check]
  auto b = [&a](int Y) -> const unsigned char *const {
    // CHECK-FIXES: {{^}}  auto b = [&a](int Y) -> unsigned char const *const {{{$}}
    return nullptr;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:37: warning: misplaced const. [voxel-const-position-check]
  auto c = [a, b](int Y) mutable -> const char *const {
    // CHECK-FIXES: {{^}}  auto c = [a, b](int Y) mutable -> char const *const {{{$}}
    return nullptr;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:33: warning: misplaced const. [voxel-const-position-check]
  auto d = [pointee](char Y) -> const auto {
    // CHECK-FIXES: {{^}}  auto d = [pointee](char Y) -> auto const {{{$}}
    return &pointee;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:20: warning: misplaced const. [voxel-const-position-check]
  auto e = []() -> const int *const *const {
    // CHECK-FIXES: {{^}}  auto e = []() -> int const *const *const {{{$}}
    return nullptr;
  };
}
