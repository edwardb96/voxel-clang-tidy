// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++11

// CHECK-MESSAGES: :[[@LINE+1]]:9: warning: misplaced const. [voxel-const-position-check]
typedef const int &Alpha;
// CHECK-FIXES: {{^}}typedef int const &Alpha;{{$}}

typedef unsigned Gamma;

// CHECK-MESSAGES: :[[@LINE+1]]:9: warning: misplaced const. [voxel-const-position-check]
typedef const Gamma &Delta;
// CHECK-FIXES: {{^}}typedef Gamma const &Delta;{{$}}
