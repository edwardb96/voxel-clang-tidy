// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++11

template <typename T>
void alpha(){};

template <typename T1, typename T2>
void beta(){};

template <typename T1, typename T2, typename T3>
void gamma(){};

template <typename... Ts>
void theta(){};

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:13: warning: misplaced const. [voxel-const-position-check]
  alpha<int const &>();
  // CHECK-FIXES: {{^}}  alpha<const int &>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+2]]:12: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:26: warning: misplaced const. [voxel-const-position-check]
  beta<int const &, char const &>();
  // CHECK-FIXES: {{^}}  beta<const int &, const char &>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+3]]:13: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:27: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:50: warning: misplaced const. [voxel-const-position-check]
  gamma<int const &, char const &, unsigned long const &>();
  // CHECK-FIXES: {{^}}  gamma<const int &, const char &, const unsigned long &>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+4]]:13: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+3]]:27: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:50: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:65: warning: misplaced const. [voxel-const-position-check]
  theta<int const &, char const &, unsigned long const &, short const &>();
  // CHECK-FIXES: {{^}}  theta<const int &, const char &, const unsigned long &, const short &>();{{$}}

  return 0;
}
