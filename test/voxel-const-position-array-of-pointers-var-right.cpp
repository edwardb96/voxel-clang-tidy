// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++11

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int *a[] = {nullptr, nullptr};
  // CHECK-FIXES: {{^}}  int const *a[] = {nullptr, nullptr};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const char *b[] = {nullptr, nullptr, nullptr};
  // CHECK-FIXES: {{^}}  char const *b[] = {nullptr, nullptr, nullptr};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int *c[]{nullptr, nullptr, nullptr, nullptr};
  // CHECK-FIXES: {{^}}  int const *c[]{nullptr, nullptr, nullptr, nullptr};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const int *e[5] = {nullptr, nullptr, nullptr, nullptr, nullptr};
  // CHECK-FIXES: {{^}}  int const *e[5] = {nullptr, nullptr, nullptr, nullptr, nullptr};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:3: warning: misplaced const. [voxel-const-position-check]
  const float *f[2];
  // CHECK-FIXES: {{^}}  float const *f[2];{{$}}

  return 0;
}
