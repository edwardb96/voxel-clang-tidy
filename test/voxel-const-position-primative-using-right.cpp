// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++11

// CHECK-MESSAGES: :[[@LINE+1]]:15: warning: misplaced const. [voxel-const-position-check]
using Alpha = const int;
// CHECK-FIXES: {{^}}using Alpha = int const;{{$}}

template<int>
// CHECK-MESSAGES: :[[@LINE+1]]:14: warning: misplaced const. [voxel-const-position-check]
using Beta = const char;
// CHECK-FIXES: {{^}}template<int>{{$}}
// CHECK-NEXT: {{^}}using Beta = char const;{{$}}

using Gamma = unsigned;

// CHECK-MESSAGES: :[[@LINE+1]]:15: warning: misplaced const. [voxel-const-position-check]
using Delta = const Gamma;
// CHECK-FIXES: {{^}}using Delta = Gamma const;{{$}}
