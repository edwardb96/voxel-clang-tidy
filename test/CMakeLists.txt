# Test runner infrastructure for Clang-based tools. This configures the Clang
# test trees for use by Lit, and delegates to LLVM's lit test handlers.
#
# Note that currently we don't support stand-alone builds of Clang, you must
# be building Clang from within a combined LLVM+Clang checkout..

set(CLANG_TOOLS_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/../../../../")
set(CLANG_TOOLS_BINARY_DIR "${CMAKE_CURRENT_BINARY_DIR}/../../../../")

set(VOXEL_MODULE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/..")
set(VOXEL_MODULE_BINARY_DIR "${CMAKE_CURRENT_BINARY_DIR}/..")

if (CMAKE_CFG_INTDIR STREQUAL ".")
  set(LLVM_BUILD_MODE ".")
else ()
  set(LLVM_BUILD_MODE "%(build_mode)s")
endif ()

string(REPLACE ${CMAKE_CFG_INTDIR} ${LLVM_BUILD_MODE} CLANG_TOOLS_DIR ${LLVM_RUNTIME_OUTPUT_INTDIR})

llvm_canonicalize_cmake_booleans(
  CLANG_ENABLE_STATIC_ANALYZER)

configure_lit_site_cfg(
  ${CMAKE_CURRENT_SOURCE_DIR}/lit.site.cfg.in
  ${CMAKE_CURRENT_BINARY_DIR}/lit.site.cfg
  )

configure_lit_site_cfg(
  ${CMAKE_CURRENT_SOURCE_DIR}/Unit/lit.site.cfg.in
  ${CMAKE_CURRENT_SOURCE_DIR}/Unit/lit.site.cfg
)

option(VOXEL_MODULE_TEST_USE_VG "Run Clang tools' tests under Valgrind" OFF)
if(VOXEL_MODULE_TEST_USE_VG)
  set(CLANG_TOOLS_TEST_EXTRA_ARGS ${CLANG_TEST_EXTRA_ARGS} "--vg")
endif()

set(VOXEL_MODULE_TEST_DEPS
  # Individual tools we test.
  clang-tidy)

if(CLANG_ENABLE_STATIC_ANALYZER)
    list(APPEND VOXEL_MODULE_TEST_DEPS
    # clang-tidy tests require it.
    clang-headers
    clang-tidy

    # Unit Tests
    VoxelClangTidyUnitTests
    )
endif()

set(llvm_utils
  FileCheck count not
  )

foreach(t ${llvm_utils})
  if(TARGET ${t})
      list(APPEND VOXEL_MODULE_TEST_DEPS ${t})
  endif()
endforeach()

add_lit_testsuite(check-voxel-tidy "Running the voxel-clang-tidy' regression tests"
  ${CMAKE_CURRENT_BINARY_DIR}
  DEPENDS ${VOXEL_MODULE_TEST_DEPS}
  ARGS ${CLANG_TOOLS_TEST_EXTRA_ARGS}
  )

set_target_properties(check-voxel-tidy PROPERTIES FOLDER "Voxel Clang Tidy Modules' tests")
