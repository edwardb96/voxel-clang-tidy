// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++11

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const a[] = {1, 2, 3, 4};
  // CHECK-FIXES: {{^}}  const int a[] = {1, 2, 3, 4};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:8: warning: misplaced const. [voxel-const-position-check]
  char const b[] = {'z', 'x', 'y'};
  // CHECK-FIXES: {{^}}  const char b[] = {'z', 'x', 'y'};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const c[]{7, 9, 2, 1};
  // CHECK-FIXES: {{^}}  const int c[]{7, 9, 2, 1};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const e[5] = {0, 0, 0, 0, 0};
  // CHECK-FIXES: {{^}}  const int e[5] = {0, 0, 0, 0, 0};{{$}}

  // CHECK-MESSAGES: :[[@LINE+1]]:9: warning: misplaced const. [voxel-const-position-check]
  float const f[2] = {1.0f, 2.5f};
  // CHECK-FIXES: {{^}}  const float f[2] = {1.0f, 2.5f};{{$}}

  return 0;
}
