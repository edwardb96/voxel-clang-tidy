// RUN: %check_clang_tidy %s voxel-const-position-check %t \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Right"}]}" -- -std=c++11

template <typename T>
void alpha(){};

template <typename T1, typename T2>
void beta(){};

template <typename T1, typename T2, typename T3>
void gamma(){};

template <typename... Ts>
void theta(){};

int main() {
  // CHECK-MESSAGES: :[[@LINE+1]]:9: warning: misplaced const. [voxel-const-position-check]
  alpha<const int *>();
  // CHECK-FIXES: {{^}}  alpha<int const *>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+2]]:8: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:20: warning: misplaced const. [voxel-const-position-check]
  beta<const char, const int *>();
  // CHECK-FIXES: {{^}}  beta<char const, int const *>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+3]]:9: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:27: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:46: warning: misplaced const. [voxel-const-position-check]
  gamma<const int *const, const char *const, const unsigned long *>();
  // CHECK-FIXES: {{^}}  gamma<int const *const, char const *const, unsigned long const *>();{{$}}

  // CHECK-MESSAGES: :[[@LINE+4]]:9: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+3]]:34: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+2]]:60: warning: misplaced const. [voxel-const-position-check]
  // CHECK-MESSAGES: :[[@LINE+1]]:89: warning: misplaced const. [voxel-const-position-check]
  theta<const int *const *const, const char *const *const, const unsigned long **const, const short *const>();
  // CHECK-FIXES: {{^}}  theta<int const *const *const, char const *const *const, unsigned long const **const, short const *const>();{{$}}

  return 0;
}
