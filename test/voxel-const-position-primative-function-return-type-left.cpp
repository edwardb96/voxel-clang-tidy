// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++14

class X {
  X(int y) {
  }

  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const a(int X) {
    // CHECK-FIXES: {{^}}  const int a(int X) {{{$}}
    return 0;
  }

  // CHECK-MESSAGES: :[[@LINE+1]]:8: warning: misplaced const. [voxel-const-position-check]
  char const b(int X) const {
    // CHECK-FIXES: {{^}}  const char b(int X) const {{{$}}
    return '-';
  }

  // CHECK-MESSAGES: :[[@LINE+1]]:7: warning: misplaced const. [voxel-const-position-check]
  int const c(int X) const;
  // CHECK-FIXES: {{^}}  const int c(int X) const;{{$}}
};

// CHECK-MESSAGES: :[[@LINE+1]]:5: warning: misplaced const. [voxel-const-position-check]
int const X::c(int X) const {
  // CHECK-FIXES: {{^}}const int X::c(int X) const {{{$}}
  return 0;
}

// CHECK-MESSAGES: :[[@LINE+1]]:6: warning: misplaced const. [voxel-const-position-check]
auto const d(char X) {
  // CHECK-FIXES: {{^}}const auto d(char X) {{{$}}
  return X;
}

// CHECK-MESSAGES: :[[@LINE+1]]:31: warning: misplaced const. [voxel-const-position-check]
auto e(const char *X) -> char const {
  // CHECK-FIXES: {{^}}auto e(const char *X) -> const char {{{$}}
  return *X;
}

// CHECK-MESSAGES: :[[@LINE+1]]:5: warning: misplaced const. [voxel-const-position-check]
int const f() {
  // CHECK-FIXES: {{^}}const int f() {{{$}}
  return 0;
}
