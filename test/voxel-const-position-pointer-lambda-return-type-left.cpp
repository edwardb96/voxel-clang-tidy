// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++14

int main() {
  int pointee = 10;

  // CHECK-MESSAGES: :[[@LINE+1]]:29: warning: misplaced const. [voxel-const-position-check]
  auto a = [](int Y) -> int const *const {
    // CHECK-FIXES: {{^}}  auto a = [](int Y) -> const int *const {{{$}}
    return nullptr;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:41: warning: misplaced const. [voxel-const-position-check]
  auto b = [&a](int Y) -> unsigned char const *const {
    // CHECK-FIXES: {{^}}  auto b = [&a](int Y) -> const unsigned char *const {{{$}}
    return nullptr;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:42: warning: misplaced const. [voxel-const-position-check]
  auto c = [a, b](int Y) mutable -> char const *const {
    // CHECK-FIXES: {{^}}  auto c = [a, b](int Y) mutable -> const char *const {{{$}}
    return nullptr;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:38: warning: misplaced const. [voxel-const-position-check]
  auto d = [pointee](char Y) -> auto const {
    // CHECK-FIXES: {{^}}  auto d = [pointee](char Y) -> const auto {{{$}}
    return &pointee;
  };

  // CHECK-MESSAGES: :[[@LINE+1]]:24: warning: misplaced const. [voxel-const-position-check]
  auto e = []() -> int const *const *const {
    // CHECK-FIXES: {{^}}  auto e = []() -> const int *const *const {{{$}}
    return nullptr;
  };
}
