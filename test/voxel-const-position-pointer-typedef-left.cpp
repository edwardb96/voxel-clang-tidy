// RUN: %check_clang_tidy %s voxel-const-position-check %t -- \
// RUN:   -config="{CheckOptions: [{key: "voxel-const-position-check.ConstPosition", value: "Left"}]}" -- -std=c++11

// CHECK-MESSAGES: :[[@LINE+1]]:13: warning: misplaced const. [voxel-const-position-check]
typedef int const *const A;
// CHECK-FIXES: {{^}}typedef const int *const A;{{$}}

// CHECK-MESSAGES: :[[@LINE+1]]:13: warning: misplaced const. [voxel-const-position-check]
typedef int const *const *const B;
// CHECK-FIXES: {{^}}typedef const int *const *const B;{{$}}

typedef int *D;

// CHECK-MESSAGES: :[[@LINE+1]]:11: warning: misplaced const. [voxel-const-position-check]
typedef D const E;
// CHECK-FIXES: {{^}}typedef const D E;{{$}}

typedef const int *F;

// CHECK-MESSAGES: :[[@LINE+1]]:11: warning: misplaced const. [voxel-const-position-check]
typedef F const G;
// CHECK-FIXES: {{^}}typedef const F G;{{$}}
