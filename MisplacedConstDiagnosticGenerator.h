#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_DIAGNOSE_MISPLACED_CONST_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_DIAGNOSE_MISPLACED_CONST_H
#include "../ClangTidy.h"
#include "ConstPosition.h"
#include "clang/AST/TypeLoc.h"
#include "clang/Lex/Token.h"
#include "llvm/ADT/StringRef.h"

namespace clang {
namespace tidy {
namespace voxel {

class MisplacedConstDiagnosticGeneratorBase {
public:
  MisplacedConstDiagnosticGeneratorBase(ClangTidyCheck &Check,
                                        ASTContext const &Context);

protected:
  DiagnosticBuilder operator()(Token BadConst, SourceLocation GoodConstLocation,
                               const TypeLoc &TypeOccurence,
                               StringRef ConstQualifierText);

private:
  CharSourceRange charRangeOf(Token Tok) const;
  ClangTidyCheck &Check;
  const ASTContext &Context;
};

template <ConstPosition PreferredConstPosition>
class MisplacedConstDiagnosticGenerator;

template <>
class MisplacedConstDiagnosticGenerator<ConstPosition::Left>
    : MisplacedConstDiagnosticGeneratorBase {
public:
  using MisplacedConstDiagnosticGeneratorBase::
      MisplacedConstDiagnosticGeneratorBase;
  DiagnosticBuilder operator()(Token BadConst, SourceLocation GoodConstLocation,
                               const TypeLoc &TypeOccurence);
};

template <>
class MisplacedConstDiagnosticGenerator<ConstPosition::Right>
    : MisplacedConstDiagnosticGeneratorBase {
public:
  using MisplacedConstDiagnosticGeneratorBase::
      MisplacedConstDiagnosticGeneratorBase;
  DiagnosticBuilder operator()(Token BadConst, SourceLocation GoodConstLocation,
                               const TypeLoc &TypeOccurence);
};

} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_DIAGNOSE_MISPLACED_CONST_H
