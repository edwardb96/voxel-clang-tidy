//===--- TemplateSpecializationNodes.h - clang-tidy--------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_TEMPLATE_SPECIALIZATION_NODES_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_TEMPLATE_SPECIALIZATION_NODES_H
#include "BadConstFinder.h"
#include "ExtractorBase.h"
#include "GetEndLoc.h"
#include "MakeIfAllHaveValues.h"
#include "PointerNodes.h"
#include "clang/AST/TypeLoc.h"
#include "llvm/ADT/Optional.h"

namespace clang {
namespace ast_matchers {
internal::Matcher<TypeLoc> classTemplateArgumentContainingConst();
internal::Matcher<Stmt> functionTemplateArgumentContainingConst();
} // namespace ast_matchers
namespace tidy {
namespace voxel {

template <typename TemplateArgumentContextNode> struct TemplateArgumentNodes {
  TemplateArgumentNodes(NodeStorage<TemplateArgumentContextNode> TemplateLoc,
                        TypeLoc ArgumentLoc)
      : TemplateLoc(TemplateLoc), ArgumentLoc(ArgumentLoc) {}

  NodeStorage<TemplateArgumentContextNode> TemplateLoc;
  TypeLoc ArgumentLoc;
};

template <typename TemplateArgumentContextNode>
const TypeLoc &
TypeLocToFix(TemplateArgumentNodes<TemplateArgumentContextNode> const &Nodes) {
  return Nodes.ArgumentLoc;
};

template <typename TemplateArgumentContextNode>
const TemplateArgumentContextNode &
ParentContext(TemplateArgumentNodes<TemplateArgumentContextNode> const &Nodes) {
  return get(Nodes.TemplateLoc);
}

template <typename TemplateArgumentContextNode, typename LinearSearcher>
llvm::Optional<Token> FindRightConst(
    LinearSearcher const &LinearSearchForConst,
    TemplateArgumentNodes<TemplateArgumentContextNode> const &Nodes) {
  return LinearSearchForConst(RightQualifierSpace(Nodes),
                              StoppingIf(&IsRightTemplateArgBoundary));
}

template <typename TemplateArgumentContextNode, typename LinearSearcher>
llvm::Optional<Token>
FindLeftConst(LinearSearcher const &LinearSearchForConst,
              TemplateArgumentNodes<TemplateArgumentContextNode> const &Nodes) {
  return LinearSearchForConst(LeftQualifierSpace(Nodes),
                              StoppingIf(&IsLeftTemplateArgBoundary));
}

template <typename TemplateArgumentContextNode, typename LinearSearcher>
llvm::Optional<Token> FindLeftConst(
    LinearSearcher const &LinearSearchForConst,
    PointerNodes<TemplateArgumentNodes<TemplateArgumentContextNode>> const
        &Nodes) {
  return LinearSearchForConst(LeftQualifierSpace(Nodes),
                              StoppingIf(&IsLeftTemplateArgBoundary));
}

using ClassTemplateArgumentNodes =
    TemplateArgumentNodes<TemplateSpecializationTypeLoc>;

class ExtractClassTemplateArgumentNodes {
public:
  llvm::Optional<ClassTemplateArgumentNodes>
  operator()(const ast_matchers::BoundNodes &BoundNodes) const;
};

using FunctionTemplateArgumentNodes = TemplateArgumentNodes<DeclRefExpr>;
class ExtractFunctionTemplateArgumentNodes {
public:
  llvm::Optional<FunctionTemplateArgumentNodes>
  operator()(const ast_matchers::BoundNodes &BoundNodes) const;
};

LeftToRightSourceTraversal<RightOfNode<TypeLoc>, SourceLocation>
RightQualifierSpace(TypeLoc ArgumentLoc, DeclRefExpr const &Ancestor);

RightToLeftSourceTraversal<SourceLocation, SourceLocation>
LeftQualifierSpace(TypeLoc ArgumentLoc, DeclRefExpr const &Ancestor);

LeftToRightSourceTraversal<RightOfNode<TypeLoc>, SourceLocation>
RightQualifierSpace(TypeLoc PointeeLoc,
                    TemplateSpecializationTypeLoc const &Ancestor);

RightToLeftSourceTraversal<SourceLocation, SourceLocation>
LeftQualifierSpace(TypeLoc PointeeLoc,
                   TemplateSpecializationTypeLoc const &Ancestor);
} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_TEMPLATE_SPECIALIZATION_NODES_H
