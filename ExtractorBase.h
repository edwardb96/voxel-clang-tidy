//===--- ExtractorBase.h - clang-tidy----------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_EXTRACTOR_BASE_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_EXTRACTOR_BASE_H
#include "../ClangTidy.h"
#include "clang/AST/TypeLoc.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
namespace clang {
namespace tidy {
namespace voxel {

template <typename AstNode>
using IsDerivedFromTypeLoc = std::is_base_of<TypeLoc, AstNode>;

template <typename Extractor>
using ResultOfExtractor = typename std::result_of<Extractor(
    const ast_matchers::BoundNodes &)>::type::value_type;

template <typename AstNode>
typename std::enable_if<!IsDerivedFromTypeLoc<AstNode>::value,
                        llvm::Optional<const AstNode *>>::type
Extract(const ast_matchers::BoundNodes &BoundNodes, llvm::StringRef ID) {
  if (auto *Node = BoundNodes.getNodeAs<AstNode>(ID))
    return Node;
  else
    return llvm::None;
}

template <typename TypeLocType>
typename std::enable_if<IsDerivedFromTypeLoc<TypeLocType>::value,
                        llvm::Optional<TypeLocType>>::type
Extract(const ast_matchers::BoundNodes &BoundNodes, llvm::StringRef ID) {
  if (auto *Node = BoundNodes.getNodeAs<TypeLoc>(ID))
    if (auto UnqualifiedLoc = Node->getUnqualifiedLoc())
      if (auto Location = UnqualifiedLoc.getAs<TypeLocType>())
        return Location;
      else
        return llvm::None;
    else
      return llvm::None;
  else
    return llvm::None;
}
} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_EXTRACTOR_BASE_H
