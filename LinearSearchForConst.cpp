#include "LinearSearchForConst.h"
#include "TokenPredicates.h"
namespace clang {
namespace tidy {
namespace voxel {
LexResult ConstOrContinue(Token Tok) {
  if (IsConst(Tok))
    return Result(Tok);
  else
    return Continue();
}
}
} // namespace tidy
} // namespace clang
