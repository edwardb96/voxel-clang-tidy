#include "MisplacedConstDiagnosticGenerator.h"
#include "SimpleLexer.h"

namespace clang {
namespace tidy {
namespace voxel {

DiagnosticBuilder MisplacedConstDiagnosticGenerator<ConstPosition::Left>::
operator()(Token BadConst, SourceLocation GoodConstLocation,
           const TypeLoc &TypeOccurence) {
  return MisplacedConstDiagnosticGeneratorBase::operator()(
      BadConst, GoodConstLocation, TypeOccurence, "const ");
}

DiagnosticBuilder MisplacedConstDiagnosticGenerator<ConstPosition::Right>::
operator()(Token BadConst, SourceLocation GoodConstLocation,
           const TypeLoc &TypeOccurence) {
  return MisplacedConstDiagnosticGeneratorBase::operator()(
      BadConst, GoodConstLocation, TypeOccurence, " const");
}

MisplacedConstDiagnosticGeneratorBase::MisplacedConstDiagnosticGeneratorBase(
    ClangTidyCheck &Check, ASTContext const &Context)
    : Check(Check), Context(Context) {}

DiagnosticBuilder MisplacedConstDiagnosticGeneratorBase::
operator()(Token BadConst, SourceLocation GoodConstLocation,
           const TypeLoc &TypeOccurence, StringRef ConstQualifierText) {
  return Check.diag(BadConst.getLocation(), "misplaced const.")
         << FixItHint::CreateRemoval(charRangeOf(BadConst))
         << FixItHint::CreateInsertion(GoodConstLocation, ConstQualifierText);
}

CharSourceRange
MisplacedConstDiagnosticGeneratorBase::charRangeOf(Token Tok) const {
  return RangeOfToken(Context, Tok);
}

} // namespace voxel
} // namespace tidy
} // namespace clang
