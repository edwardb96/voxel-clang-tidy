//===--- ASTMatchersInternal.h - clang-tidy----------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_VOXEL_ASTMATCHERSINTERNAL_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_VOXEL_ASTMATCHERSINTERNAL_H
#include "clang/AST/Decl.h"
#include "clang/AST/ExprCXX.h"
#include "clang/AST/Type.h"
#include "clang/ASTMatchers/ASTMatchersInternal.h"
#include "llvm/ADT/Optional.h"
namespace clang {
namespace ast_matchers {
namespace internal {

template <typename T> llvm::Optional<TypeLoc> DeclaredTypeLoc(const T &Node) {
  const TypeSourceInfo *TSI = Node.getTypeSourceInfo();
  if (TSI != nullptr) {
    auto DeclaredTypeLoc = TSI->getTypeLoc().IgnoreParens();
    if (DeclaredTypeLoc)
      return DeclaredTypeLoc;
    else
      return llvm::None;
  } else {
    return llvm::None;
  }
}

llvm::Optional<TypeLoc> ReturnTypeLoc(const FunctionDecl &Node);
llvm::Optional<TypeLoc> TypeLocIfTypeArg(const TemplateArgumentLoc &Node);
bool IsLocallyConstQualified(const QualType &Type);

template <typename TypeLocType> bool UnqualifiedCanCastTo(TypeLoc const &Node) {
  auto UnqualifiedNode = Node.getUnqualifiedLoc();
  if (!UnqualifiedNode.isNull())
    return !UnqualifiedNode.getAs<TypeLocType>().isNull();
  else
    return false;
}
} // namespace internal
} // namespace ast_matchers
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_VOXEL_ASTMATCHERSINTERNAL_H
