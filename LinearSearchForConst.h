//===--- LinearSearchForConst.h - clang-tidy---------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_LINEAR_SEARCH_FOR_CONST_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_LINEAR_SEARCH_FOR_CONST_H
#include "GetEndLoc.h"
#include "SimpleLexer.h"
#include "SourceLocations.h"
#include "SourceTraversal.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/TypeLoc.h"
#include "clang/Basic/LLVM.h"
#include "clang/Lex/Token.h"
#include "llvm/ADT/Optional.h"

namespace clang {
namespace tidy {
namespace voxel {

template <typename AstNode> SourceLocation LeftOf(AstNode const &Node) {
  return Node.getLocStart();
}

template <typename AstNode> RightOfNode<AstNode> RightOf(AstNode const &Node) {
  return RightOfNode<AstNode>(NodeStorage<AstNode>(Node));
}

template <typename TokenPredicate> class EarlyStopCondition {
public:
  EarlyStopCondition(TokenPredicate ShouldStop) : ShouldStop(ShouldStop) {}
  bool operator()(Token Tok) { return ShouldStop(Tok); }

private:
  TokenPredicate ShouldStop;
};

template <typename TokenPredicate>
EarlyStopCondition<TokenPredicate> StoppingIf(TokenPredicate ShouldStop) {
  return EarlyStopCondition<TokenPredicate>(ShouldStop);
}

LexResult ConstOrContinue(Token Tok);

template <typename LexerFactory> class LinearSearchForConst {
public:
  LinearSearchForConst(const LexerFactory &MakeLexerAt,
                       const ASTContext &Context)
      : Context(Context), MakeLexerAt(MakeLexerAt) {}

  template <typename StartLocation, typename TokenPredicate>
  llvm::Optional<Token>
  operator()(UnboundedLeftToRightSourceTraversal<StartLocation> Traversal,
             EarlyStopCondition<TokenPredicate> ShouldStop) const {
    auto Start = Traversal.start(Context);
    return LexWhile(MakeLexerAt, Start,
                    [&ShouldStop](Token Tok) -> LexResult {
                      if (ShouldStop(Tok))
                        return Stop();
                      else
                        return ConstOrContinue(Tok);
                    });
  }

  template <typename StartLocation, typename EndLocation>
  llvm::Optional<Token>
  operator()(LeftToRightSourceTraversal<StartLocation, EndLocation> Traversal) const {
    return LexBetween(MakeLexerAt, Context, Traversal.range(Context),
                      &ConstOrContinue);
  }

  template <typename StartLocation, typename EndLocation,
            typename TokenPredicate>
  llvm::Optional<Token>
  operator()(LeftToRightSourceTraversal<StartLocation, EndLocation> Traversal,
             EarlyStopCondition<TokenPredicate> ShouldStop) const {
    auto Range = Traversal.range(Context);
    return LexBetween(MakeLexerAt, Context, Range,
                      [&ShouldStop](Token Tok) -> LexResult {
                        if (ShouldStop(Tok))
                          return Stop();
                        else
                          return ConstOrContinue(Tok);
                      });
  }

  template <typename StartLocation, typename EndLocation>
  llvm::Optional<Token> operator()(
      RightToLeftSourceTraversal<StartLocation, EndLocation> Traversal) const {
    return ReverseLexBetween(MakeLexerAt, Context, Traversal.range(Context),
                             &ConstOrContinue);
  }

  template <typename StartLocation, typename EndLocation,
            typename TokenPredicate>
  llvm::Optional<Token>
  operator()(RightToLeftSourceTraversal<StartLocation, EndLocation> Traversal,
             EarlyStopCondition<TokenPredicate> ShouldStop) const {
    return ReverseLexBetween(MakeLexerAt, Context, Traversal.range(Context),
                             [&ShouldStop](Token Tok) -> LexResult {
                               if (ShouldStop(Tok))
                                 return Stop();
                               else
                                 return ConstOrContinue(Tok);
                             });
  }

private:
  const ASTContext &Context;
  LexerFactory MakeLexerAt;
};

template <typename LexerFactory>
LinearSearchForConst<LexerFactory>
MakeLinearSearcherForConst(const LexerFactory &MakeLexerAt,
                           const ASTContext &Context) {
  return LinearSearchForConst<LexerFactory>(MakeLexerAt, Context);
}

template <typename AncestorNode>
RightToLeftSourceTraversal<SourceLocation, SourceLocation>
LeftQualifierSpace(TypeLoc Loc, AncestorNode const &Ancestor) {
  return RightToLeft(LeftOf(Loc), LeftOf(Ancestor));
}

template <typename AncestorNode>
LeftToRightSourceTraversal<RightOfNode<TypeLoc>, RightOfNode<AncestorNode>>
RightQualifierSpace(TypeLoc Loc, AncestorNode const &Ancestor) {
  return LeftToRight(RightOf(Loc), RightOf(Ancestor));
}

} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_LINEAR_SEARCH_FOR_CONST_H
