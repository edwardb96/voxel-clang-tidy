#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_END_CHAR_LOC_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_END_CHAR_LOC_H
#include "clang/AST/ASTContext.h"
#include "clang/Basic/SourceLocation.h"
#include "clang/Lex/Lexer.h"
namespace clang {
namespace tidy {
namespace voxel {

SourceLocation OnePastEndOfTokenAt(const ASTContext& Context, SourceLocation Token);

template <typename ASTNode>
SourceLocation EndCharLoc(const ASTContext &Context, const ASTNode &Node) {
  return OnePastEndOfTokenAt(Context, Node.getLocEnd());
}

SourceLocation EndCharLoc(const ASTContext &Context, const TypeLoc &Node);

} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_END_CHAR_LOC_H
