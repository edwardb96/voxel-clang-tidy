//===--- LinearSearchForConst.h - clang-tidy---------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_LAZY_SOURCE_LOCATIONS_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_LAZY_SOURCE_LOCATIONS_H
#include "GetEndLoc.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/Expr.h"
#include "clang/Basic/LLVM.h"
#include <type_traits>

namespace clang {
namespace tidy {
namespace voxel {

template <typename AstNode>
using IsDerivedFromTypeLoc = std::is_base_of<TypeLoc, AstNode>;

template <typename AstNode>
using NodeStorageInternal =
    typename std::conditional<IsDerivedFromTypeLoc<AstNode>::value,
                              AstNode const, AstNode const *>::type;

template <typename AstNode,
          bool IsTypeLoc = IsDerivedFromTypeLoc<AstNode>::value>
class NodeStorageImpl;

template <typename AstNode> class NodeStorageImpl<AstNode, true> {
public:
  NodeStorageImpl(AstNode Node) : Node(Node) {}
  AstNode const &get() const { return Node; }

private:
  AstNode Node;
};

template <typename AstNode> class NodeStorageImpl<AstNode, false> {
public:
  NodeStorageImpl(AstNode const *Node) : Node(Node) {}
  NodeStorageImpl(AstNode const &Node) : Node(&Node) {}
  AstNode const &get() const { return *Node; }

private:
  AstNode const *Node;
};

template <typename AstNode>
using ByPointerNodeStorage = NodeStorageImpl<AstNode, false>;

template <typename AstNode>
using ByValueNormalNodeStorage = NodeStorageImpl<AstNode, true>;

template <typename AstNode>
using NodeStorage =
    NodeStorageImpl<AstNode, IsDerivedFromTypeLoc<AstNode>::value>;

template <typename AstNode, bool IsTypeLoc>
AstNode const& get(NodeStorageImpl<AstNode, IsTypeLoc> const& Node) {
  return Node.get();
}

template <typename AstNode> class RightOfNode {
public:
  RightOfNode(NodeStorage<AstNode> Node) : Node(Node) {}

  SourceLocation getSourceLocation(ASTContext const &Context) const {
    return EndCharLoc(Context, get(Node));
  }

private:
  NodeStorage<AstNode> Node;
};

} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_LAZY_SOURCE_LOCATIONS_H
