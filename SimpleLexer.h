//===--- SimpleLexer.h - clang-tidy------------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_SIMPLE_LEXER_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_SIMPLE_LEXER_H

#include "clang/AST/ASTContext.h"
#include "clang/Basic/SourceLocation.h"
#include "clang/Lex/Lexer.h"
#include "clang/Lex/Token.h"
#include "llvm/ADT/StringRef.h"
#include <memory>

namespace clang {
namespace tidy {
namespace voxel {

class SimpleLexer {
public:
  SimpleLexer(SourceLocation StartHere, const ASTContext &Context);
  llvm::Optional<Token> nextToken();

private:
  Token withIdentifierInfo(Token Tok) const;
  StringRef identifierOf(Token Tok) const;

  using ClangLexer_uptr = std::unique_ptr<Lexer>;
  static ClangLexer_uptr makeLexerStartingAt(const SourceManager &Sources,
                                             const LangOptions &LangOpts,
                                             SourceLocation StartSource);
  static StringRef identifierOfToken(const SourceManager &Sources, Token Tok);
  const LangOptions &getLangOpts() const;
  const ASTContext &getContext() const;
  const SourceManager &getSources() const;

  ClangLexer_uptr RawLexerPtr;
  Lexer &RawLexer;
  const ASTContext &Context;
};

class SimpleLexerFactory {
public:
  SimpleLexerFactory(const ASTContext &Context);
  SimpleLexer operator()(SourceLocation Loc) const;

private:
  const ASTContext &Context;
};

enum class NextAction { Abort, Continue };

class LexResult {
public:
  LexResult(NextAction WhatNext);
  LexResult(Token Tok);
  bool done() const;
  llvm::Optional<Token> tokenElseNone() const;

private:
  llvm::Optional<Token> Tok;
  NextAction WhatNext;
};

LexResult Stop();
LexResult Continue();
LexResult Result(Token Tok);

bool IsBefore(const ASTContext &Context, SourceLocation First,
              SourceLocation Second);

template <typename SimpleLexer, typename Do>
llvm::Optional<Token> LexWhile(SimpleLexer &Lexer, const Do &Action) {
  auto Tok = Lexer.nextToken();
  for (; Tok.hasValue(); Tok = Lexer.nextToken()) {
    auto Result = Action(Tok.getValue());
    if (Result.done())
      return Result.tokenElseNone();
  }
  return llvm::None;
}

template <typename LexerFactory, typename Do>
llvm::Optional<Token> LexWhile(const LexerFactory &MakeLexerAt,
                               SourceLocation Begin, const Do &Action) {
  auto Lexer = MakeLexerAt(Begin);
  return LexWhile(Lexer, Action);
}

template <typename LexerFactory, typename Do>
llvm::Optional<Token> LexBetween(const LexerFactory &MakeLexerAt,
                                 const ASTContext &Context, SourceRange Range,
                                 const Do &Action) {
  auto const End = Range.getEnd();
  return LexWhile(MakeLexerAt, Range.getBegin(),
                  [End, &Context, &Action](Token Tok) -> LexResult {
                    if (IsBefore(Context, Tok.getLocation(), End))
                      return Action(Tok);
                    else
                      return Stop();
                  });
}

template <typename LexerFactory>
void LexTokensToBuffer(const LexerFactory &MakeLexerAt,
                       const ASTContext &Context, SourceRange Range,
                       SmallVectorImpl<Token> &Output) {
  LexBetween(MakeLexerAt, Context, Range, [&Output](Token Tok) -> LexResult {
    Output.emplace_back(Tok);
    return Continue();
  });
}

template <int StackBufferSize = 10, typename LexerFactory, typename Do>
llvm::Optional<Token> ReverseLexBetween(const LexerFactory &MakeLexerAt,
                                        const ASTContext &Context,
                                        SourceRange Range, Do Action) {
  SmallVector<Token, StackBufferSize> Tokens;
  LexTokensToBuffer(MakeLexerAt, Context, Range, Tokens);
  for (auto It = Tokens.rbegin(); It != Tokens.rend(); It++) {
    auto Tok = *It;
    auto Result = Action(Tok);
    if (Result.done())
      return Result.tokenElseNone();
  }
  return llvm::None;
}

CharSourceRange FileCharRange(const ASTContext &Context, CharSourceRange Range);

CharSourceRange RangeOfToken(const ASTContext &Context, Token Token);

CharSourceRange RangeExcludingStart(const ASTContext &Context,
                                    SourceLocation Start, SourceLocation End);

CharSourceRange RangeExcludingEnd(const ASTContext &Context,
                                  SourceLocation Start, SourceLocation End);

} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_SIMPLE_LEXER_H
