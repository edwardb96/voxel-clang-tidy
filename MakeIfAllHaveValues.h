//===--- MakeIfAllHaveValues.h - clang-tidy----------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_MAKE_IF_ALL_HAVE_VALUES_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_MAKE_IF_ALL_HAVE_VALUES_H
namespace clang {
namespace tidy {
namespace voxel {
template <typename T1> bool AllHaveValues(llvm::Optional<T1> First) {
  return First.hasValue();
}

template <typename T1, typename T2, typename... Ts>
bool AllHaveValues(llvm::Optional<T1> First, llvm::Optional<T2> Second,
                   llvm::Optional<Ts>... Rest) {
  return First.hasValue() && AllHaveValues(Second, Rest...);
}

template <typename Nodes, typename... Ts>
auto MakeIfAllHaveValues(llvm::Optional<Ts>... Params)
    -> llvm::Optional<Nodes> {
  if (AllHaveValues(Params...))
    return Nodes(Params.getValue()...);
  else
    return llvm::None;
}
} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_MAKE_IF_ALL_HAVE_VALUES_H
