//===--- TokenPredicates.h - clang-tidy--------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_TOKEN_PREDICATES_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_TOKEN_PREDICATES_H
#include "clang/Lex/Token.h"
namespace clang {
namespace tidy {
namespace voxel {

bool IsConst(Token Tok);
bool IsLeftTypeEdge(Token Tok);
bool IsLeftReturnTypeEdge(Token Tok);
bool IsLeftTemplateArgBoundary(Token Tok);
bool IsRightTypeEdge(Token Tok);
bool IsRightReturnTypeEdge(Token Tok);
bool IsRightTemplateArgBoundary(Token Tok);

} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_TOKEN_PREDICATES_H
