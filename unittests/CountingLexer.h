//===--- CountingLexer.h - clang-tidy----------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_ARRAY_NODES_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_ARRAY_NODES_H
#include "../SimpleLexer.h"
namespace clang {
namespace tidy {
namespace voxel {
class CountingSimpleLexer {
public:
  CountingSimpleLexer(SimpleLexer RealLexer, int &CallCount);
  llvm::Optional<Token> nextToken();

private:
  SimpleLexer RealLexer;
  int &CallCount;
};

class CountingLexerFactory {
public:
  CountingLexerFactory(const ASTContext &Context, int &CallCount);
  CountingSimpleLexer operator()(SourceLocation Loc) const;

private:
  const ASTContext &Context;
  int &CallCount;
};
} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_ARRAY_NODES_H
