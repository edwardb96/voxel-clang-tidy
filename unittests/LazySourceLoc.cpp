#include "LazySourceLoc.h"
namespace clang {
namespace tidy {
namespace voxel {
SourceLocation FileLineColToSourceLocation(const ASTContext &Context, Line L,
                                           Column C) {
  llvm::errs() << "******************  Translating Loc " << L.LineNumber << ":"
               << C.ColumnNumber << '\n';
  auto &SourceManager = Context.getSourceManager();
  auto &FileManager = SourceManager.getFileManager();
  auto *FileEntry = FileManager.getFile("input.cc");
  return SourceManager.translateFileLineCol(FileEntry, L.LineNumber,
                                            C.ColumnNumber);
}
} // namespace voxel
} // namespace tidy
} // namespace clang
