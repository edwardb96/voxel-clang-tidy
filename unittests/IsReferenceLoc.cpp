#include "../../../../../unittests/ASTMatchers/ASTMatchersTest.h"
#include "../ASTMatchers.h"
#include <gtest/gtest.h>

namespace clang {
namespace tidy {
namespace voxel {
using namespace ast_matchers;

TEST(IsReferenceLoc, MatchesSimpleCase) {
  auto Matcher = typeLoc(isReferenceLoc());
  EXPECT_TRUE(matches("int const& x = 0;", Matcher));
}

TEST(IsReferenceLoc, MatchesDeduced) {
  auto Matcher = typeLoc(isReferenceLoc());
  EXPECT_TRUE(matches("auto const& x = 0;", Matcher));
}

TEST(IsReferenceLoc, MatchesTemplateParam) {
  auto Matcher = typeLoc(isReferenceLoc());
  EXPECT_TRUE(matches("template<typename T> void f(T& x) {}", Matcher));
}

TEST(IsReferenceLoc, MatchesForReferenceOnly) {
  auto Matcher = typeLoc(isReferenceLoc()).bind("loc");
  EXPECT_TRUE(matchAndVerifyResultTrue(
      "int* y = 0; int const& x = 1;", Matcher,
      llvm::make_unique<VerifyIdIsBoundTo<TypeLoc>>("loc", 1)));
}

} // namespace voxel
} // namespace tidy
} // namespace clang
