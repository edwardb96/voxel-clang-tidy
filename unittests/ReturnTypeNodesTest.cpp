#include "../ReturnTypeNodes.h"
#include "../LinearSearchForConst.h"
#include "../SimpleLexer.h"
#include "IsSourceRangeMatcher.h"
#include "MockLinearSearchForConst.h"
#include "VerifyNodes.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>
namespace clang {
namespace tidy {
namespace voxel {
using ::testing::_;

class ReturnTypeTest : public ::testing::Test {
public:
  ReturnTypeTest() : LexedTokenCount(0) {}

  LinearSearchForConst<CountingLexerFactory>
  tokenCountingSearcher(const ASTContext &Context) {
    LexedTokenCount = 0;
    return MakeLinearSearcherForConst(
        CountingLexerFactory(Context, LexedTokenCount), Context);
  }

  MockLinearSearchForConst const &mockSearcher(const ASTContext &Context) {
    MockSearch = llvm::make_unique<MockLinearSearchForConst>(Context);
    return *MockSearch;
  }

  testing::AssertionResult extractedFromSource(
      llvm::StringRef Code,
      std::unique_ptr<ast_matchers::BoundNodesCallback> Callback) {
    return matchAndVerifyResultTrue(
        Code, ast_matchers::functionDeclReturnTypeContainingConst(),
        std::move(Callback));
  }

  template <typename PerMatchCallback>
  std::unique_ptr<ast_matchers::VerifyBoundNodes> is(PerMatchCallback OnMatch,
                                                     int ExpectedCount = 1) {
    return ast_matchers::VerifyExtractedNodes(ExtractFunctionReturnTypeNodes(),
                                              OnMatch, ExpectedCount);
  }

protected:
  std::unique_ptr<MockLinearSearchForConst> MockSearch;
  int LexedTokenCount;
};

TEST_F(ReturnTypeTest, ExtractsConstReturnType) {
  EXPECT_TRUE(extractedFromSource(
      "int const f(){ return 0; }",
      ast_matchers::VerifyNodes([](ast_matchers::BoundNodes const &Nodes,
                                   ASTContext const &Context) {
        auto Extract = ExtractFunctionReturnTypeNodes();
        auto ContextNodes = Extract(Nodes);
        EXPECT_TRUE(ContextNodes.hasValue());
        EXPECT_TRUE(HasBoundNodeEqualTo(TypeLocToFix(ContextNodes.getValue()),
                                        "loc", Nodes));
        EXPECT_TRUE(HasBoundNodeEqualTo(&ParentContext(ContextNodes.getValue()),
                                        "decl", Nodes));
      })));
}

TEST_F(ReturnTypeTest, FindsRightConstInReturnTypeSimple) {
  EXPECT_TRUE(extractedFromSource(
      "int const f(){ return 0; }",
      is([this](FunctionReturnTypeNodes const &Nodes,
                ASTContext const &Context) {
        auto MaybeBadConst =
            FindRightConst(tokenCountingSearcher(Context), Nodes);
        ASSERT_TRUE(MaybeBadConst.hasValue());
        auto BadConst = MaybeBadConst.getValue();
        EXPECT_EQ(Loc(Line(1), Column(5)).in(Context), BadConst.getLocation());
      })));
}

TEST_F(ReturnTypeTest, FindsNoRightConstWhenOnLeft) {
  EXPECT_TRUE(extractedFromSource(
      "const int f(){ return 0; }",
      is([this](FunctionReturnTypeNodes const &Nodes,
                ASTContext const &Context) {
        auto MaybeBadConst =
            FindRightConst(tokenCountingSearcher(Context), Nodes);
        EXPECT_FALSE(MaybeBadConst.hasValue());
      })));
}

TEST_F(ReturnTypeTest, LexesOnlyOneTokenToFindConstOnImmediateRight) {
  EXPECT_TRUE(extractedFromSource(
      "int const f(){ return 0; }",
      is([this](FunctionReturnTypeNodes const &Nodes,
                ASTContext const &Context) {
        FindRightConst(tokenCountingSearcher(Context), Nodes);
        EXPECT_EQ(1, LexedTokenCount);
      })));
}

TEST_F(ReturnTypeTest, LexesOnlyOneTokenToFindConstOnImmediateLeft) {
  EXPECT_TRUE(extractedFromSource(
      "const int x = 10;\n"
      "const int f(){ return 0; }",
      is([this](FunctionReturnTypeNodes const &Nodes,
                ASTContext const &Context) {
        FindRightConst(tokenCountingSearcher(Context), Nodes);
        EXPECT_EQ(1, LexedTokenCount);
      })));
}

TEST_F(ReturnTypeTest, FindLeftConstPerformsCorrectTraversal) {
  EXPECT_TRUE(extractedFromSource(
      "const int x = 10;\n"
      "const int f(){ return 0; }",
      is([this](FunctionReturnTypeNodes const &Nodes,
                ASTContext const &Context) {
        auto &SearchForConst = mockSearcher(Context);
        EXPECT_CALL(
            SearchForConst,
            RightToLeft(IsSourceRange(Context, Range(Loc(Line(2), Column(1)),
                                                     Loc(Line(2), Column(7)))),
                        _));
        FindLeftConst(SearchForConst, Nodes);
      })));
}

TEST_F(ReturnTypeTest, FindRightConstPerformsCorrectTraversal) {
  EXPECT_TRUE(extractedFromSource(
      "const int x = 10;\n"
      "const int f(){ return 0; }\n"
      "int y = 0;",
      is([this](FunctionReturnTypeNodes const &Nodes,
                ASTContext const &Context) {
        auto &SearchForConst = mockSearcher(Context);
        EXPECT_CALL(
            SearchForConst,
            LeftToRight(IsSourceRange(Context, Range(Loc(Line(2), Column(10)),
                                                     Loc(Line(2), Column(27)))),
                        _));
        FindRightConst(SearchForConst, Nodes);
      })));
}
} // namespace voxel
} // namespace tidy
} // namespace clang
