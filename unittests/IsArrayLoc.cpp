#include "../../../../../unittests/ASTMatchers/ASTMatchersTest.h"
#include "../ASTMatchers.h"
#include <gtest/gtest.h>

namespace clang {
namespace tidy {
namespace voxel {
using namespace ast_matchers;

TEST(IsArrayLoc, MatchesSimpleCase) {
  auto Matcher = typeLoc(isArrayLoc());
  EXPECT_TRUE(matches("int x[] = {1, 2, 3};", Matcher));
}

TEST(IsArrayLoc, MatchesNestedArray) {
  auto Matcher = typeLoc(isArrayLoc());
  EXPECT_TRUE(matches("int x[3][3] = {};", Matcher));
}

TEST(IsArrayLoc, MatchesTemplateParam) {
  auto Matcher = typeLoc(isArrayLoc());
  EXPECT_TRUE(
      matches("template<typename T> void f(T x[5]) { }", Matcher));
}

} // namespace voxel
} // namespace tidy
} // namespace clang
