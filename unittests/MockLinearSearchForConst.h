#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_MOCK_LINEAR_SEARCH_FOR_CONST_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_MOCK_LINEAR_SEARCH_FOR_CONST_H
namespace clang {
namespace tidy {
namespace voxel {

class MockLinearSearchForConst {
public:
  using TokenPredicateFunction = std::function<bool(Token)>;
  MockLinearSearchForConst(const ASTContext &Context) : Context(Context) {}
  MOCK_CONST_METHOD2(UnboundedLeftToRight,
                     llvm::Optional<Token>(SourceLocation,
                                           TokenPredicateFunction));

  MOCK_CONST_METHOD1(LeftToRight, llvm::Optional<Token>(SourceRange));
  MOCK_CONST_METHOD2(LeftToRight,
                     llvm::Optional<Token>(SourceRange,
                                           TokenPredicateFunction));

  MOCK_CONST_METHOD1(RightToLeft, llvm::Optional<Token>(SourceRange));
  MOCK_CONST_METHOD2(RightToLeft,
                     llvm::Optional<Token>(SourceRange,
                                           TokenPredicateFunction));

  template <typename StartLocation, typename TokenPredicate>
  llvm::Optional<Token>
  operator()(UnboundedLeftToRightSourceTraversal<StartLocation> Traversal,
             EarlyStopCondition<TokenPredicate> ShouldStop) const {
    return UnboundedLeftToRight(Traversal.start(Context),
                         TokenPredicateFunction(ShouldStop));
  }

  template <typename StartLocation, typename EndLocation>
  llvm::Optional<Token> operator()(
      LeftToRightSourceTraversal<StartLocation, EndLocation> Traversal) const {
    return LeftToRight(Traversal.range(Context));
  }

  template <typename StartLocation, typename EndLocation,
            typename TokenPredicate>
  llvm::Optional<Token>
  operator()(LeftToRightSourceTraversal<StartLocation, EndLocation> Traversal,
             EarlyStopCondition<TokenPredicate> ShouldStop) const {
    return LeftToRight(Traversal.range(Context),
                       TokenPredicateFunction(ShouldStop));
  }

  template <typename StartLocation, typename EndLocation>
  llvm::Optional<Token> operator()(
      RightToLeftSourceTraversal<StartLocation, EndLocation> Traversal) const {
    return RightToLeft(Traversal.range(Context));
  }

  template <typename StartLocation, typename EndLocation,
            typename TokenPredicate>
  llvm::Optional<Token>
  operator()(RightToLeftSourceTraversal<StartLocation, EndLocation> Traversal,
             EarlyStopCondition<TokenPredicate> ShouldStop) const {
    return RightToLeft(Traversal.range(Context),
                       TokenPredicateFunction(ShouldStop));
  }

private:
  const ASTContext &Context;
};
}
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_MOCK_LINEAR_SEARCH_FOR_CONST_H
