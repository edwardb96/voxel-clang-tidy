#include "IsSourceRangeMatcher.h"
namespace clang {
void PrintTo(const SourceRange &value, ::std::ostream *os) {
  if (value.isValid()) {
    *os << "source range <is valid>";
  } else {
    *os << "source range <is not valid>";
  }
}
namespace tidy {
namespace voxel {

using ::testing::MakeMatcher;
using ::testing::MatchResultListener;
using ::testing::Matcher;
using ::testing::MatcherInterface;

IsSourceRangeMatcher::IsSourceRangeMatcher(const ASTContext &Context,
                                           const Range &ExpectedRange)
    : Context(Context), ExpectedRange(ExpectedRange.in(Context)) {}

bool IsSourceRangeMatcher::MatchAndExplain(
    SourceRange ActualRange, MatchResultListener *Listener) const {
  *Listener << "actual range is "
            << ActualRange.getBegin().printToString(Context.getSourceManager())
            << " to "
            << ActualRange.getEnd().printToString(Context.getSourceManager());
  return ExpectedRange == ActualRange;
}

void IsSourceRangeMatcher::DescribeTo(::std::ostream *os) const {
  *os << "is equal to the source range "
      << ExpectedRange.getBegin().printToString(Context.getSourceManager())
      << " to "
      << ExpectedRange.getEnd().printToString(Context.getSourceManager());
}

void IsSourceRangeMatcher::DescribeNegationTo(::std::ostream *os) const {
  *os << "is not equal the source range "
      << ExpectedRange.getBegin().printToString(Context.getSourceManager())
      << " to "
      << ExpectedRange.getEnd().printToString(Context.getSourceManager());
}

Matcher<SourceRange> IsSourceRange(const ASTContext &Context,
                                   const Range &Range) {
  return MakeMatcher(new IsSourceRangeMatcher(Context, Range));
}
} // namespace voxel
} // namespace tidy
} // namespace clang
