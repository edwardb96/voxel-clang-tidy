#include "../../../../../unittests/ASTMatchers/ASTMatchersTest.h"
#include "../ASTMatchers.h"
#include <gtest/gtest.h>

namespace clang {
namespace tidy {
namespace voxel {
using namespace ast_matchers;

TEST(IsLocallyConstQualified, MatchesWhenConst) {
  auto Matcher = qualType(isLocallyConstQualified());
  EXPECT_TRUE(matches("const int x = 0;", Matcher));
  EXPECT_TRUE(matches("int const x(){ return 0; };", Matcher));
}

TEST(IsLocallyConstQualified, DoesNotMatchWhenNotConst) {
  auto Matcher = qualType(isLocallyConstQualified());
  EXPECT_TRUE(notMatches("int x = 0;", Matcher));
  EXPECT_TRUE(notMatches("int x(){ return 0; };", Matcher));
}

TEST(IsLocallyConstQualified, DoesNotMatchWhenConstButNotLocal) {
  auto Matcher = qualType(isLocallyConstQualified()).bind("type");
  EXPECT_TRUE(matchAndVerifyResultTrue(
      "using X = const int;\n"
      "X y = 10;",
      Matcher, llvm::make_unique<VerifyIdIsBoundTo<QualType>>("type", 1)));
}

TEST(IsLocallyConstQualified, DoesNotMatchWhenConstButDeduced) {
  auto Matcher = qualType(isLocallyConstQualified()).bind("type");
  EXPECT_TRUE(matchAndVerifyResultTrue(
      "using X = const int;\n"
      "auto x = X(10);",
      Matcher, llvm::make_unique<VerifyIdIsBoundTo<QualType>>("type", 1)));
}

} // namespace voxel
} // namespace tidy
} // namespace clang
