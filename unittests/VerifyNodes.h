//===- unittest/Tooling/ASTMatchersTest.h - Matcher tests helpers ------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_VERIFY_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_VERIFY_H

#include "../../../../../unittests/ASTMatchers/ASTMatchersTest.h"
#include "CountingLexer.h"

namespace clang {
namespace tidy {
namespace voxel {
} // namespace voxel
} // namespace tidy
namespace ast_matchers {

class VerifyBoundNodes : public BoundNodesCallback {
public:
  using PerMatchCallback =
      std::function<void(const BoundNodes &, const ASTContext &)>;

  VerifyBoundNodes(PerMatchCallback OnMatch, int ExpectedCount = -1)
      : ExpectedCount(-1), Count(0), OnMatch(std::move(OnMatch)) {}

  void onEndOfTranslationUnit() override {
    if (ExpectedCount != -1) {
      EXPECT_EQ(ExpectedCount, Count);
    }
    Count = 0;
  }

  ~VerifyBoundNodes() override { EXPECT_EQ(0, Count); }

  bool run(const BoundNodes *Nodes, ASTContext *Context) override {
    OnMatch(*Nodes, *Context);
    ++Count;
    return true;
  }

  bool run(const BoundNodes *Nodes) override {
    llvm::errs() << "Calling VerifyBoundNodes without Context is not "
                    "supported.\n";
    return false;
  }

private:
  const int ExpectedCount;
  int Count;
  PerMatchCallback OnMatch;
};

inline std::unique_ptr<VerifyBoundNodes>
VerifyNodes(typename VerifyBoundNodes::PerMatchCallback NodeMatches,
            int ExpectedCount = 1) {
  return llvm::make_unique<VerifyBoundNodes>(std::move(NodeMatches),
                                             ExpectedCount);
}

template <typename Extractor, typename PerMatchCallback>
std::unique_ptr<VerifyBoundNodes> VerifyExtractedNodes(const Extractor &Extract,
                                                       PerMatchCallback OnMatch,
                                                       int ExpectedCount = 1) {
  return VerifyNodes(
      [OnMatch, &Extract](BoundNodes const &Nodes, ASTContext const &Context) {
        auto ExtractedNodes = Extract(Nodes);
        if (ExtractedNodes.hasValue())
          OnMatch(ExtractedNodes.getValue(), Context);
        else
          FAIL() << "Failed to extract nodes from match result.";
      });
}

inline bool HasNodeBoundTo(StringRef Id, BoundNodes const &ActualNodes) {
  auto const &Map = ActualNodes.getMap();
  auto It = Map.find(Id);
  return It != Map.cend();
}

template <typename AstNode>
testing::AssertionResult HasBoundNodeEqualTo(AstNode const *Expected,
                                             StringRef Id,
                                             BoundNodes const &ActualNodes) {
  auto MaybeNode = ActualNodes.getNodeAs<AstNode>(Id);
  if (MaybeNode != nullptr) {
    if (Expected == MaybeNode)
      return testing::AssertionSuccess();
    else
      return testing::AssertionFailure()
             << '\'' << Id << '\'' << " was bound to a different node.";
  } else {
    if (HasNodeBoundTo(Id, ActualNodes))
      return testing::AssertionFailure()
             << '\'' << Id << '\''
             << " was bound to a node which has the wrong type.";
    else
      return testing::AssertionFailure()
             << '\'' << Id << '\'' << " was not bound to any node";
  }
}

template <typename AstNode>
using IsDerivedFromTypeLoc = std::is_base_of<TypeLoc, AstNode>;

template <typename AstNode, typename = typename std::enable_if<
                                IsDerivedFromTypeLoc<AstNode>::value>::type>
testing::AssertionResult HasBoundNodeEqualTo(AstNode const &Expected,
                                             StringRef Id,
                                             BoundNodes const &ActualNodes) {
  auto MaybeNode = ActualNodes.getNodeAs<TypeLoc>(Id);
  if (MaybeNode != nullptr) {
    auto MaybeDerivedTypeLoc = MaybeNode->getUnqualifiedLoc().getAs<AstNode>();
    if (Expected == MaybeDerivedTypeLoc)
      return testing::AssertionSuccess();
    else
      return testing::AssertionFailure()
             << '\'' << Id << '\''
             << " was bound to a different node which may be a type loc.";
  } else {
    if (HasNodeBoundTo(Id, ActualNodes))
      return testing::AssertionFailure()
             << '\'' << Id << '\''
             << " was bound to a node which is not a type loc.";
    else
      return testing::AssertionFailure()
             << '\'' << Id << '\''
             << " was not bound to any node even a type loc.";
  }
}

} // namespace ast_matchers
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_VERIFY_H
