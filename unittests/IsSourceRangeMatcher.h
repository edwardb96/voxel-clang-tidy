#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_MOCK_IS_SOURCE_RANGE_MATCHER_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_MOCK_IS_SOURCE_RANGE_MATCHER_H
#include "LazySourceLoc.h"
#include "gmock/gmock.h"

namespace clang {
void PrintTo(const SourceRange &value, ::std::ostream *os);
namespace tidy {
namespace voxel {
class IsSourceRangeMatcher : public ::testing::MatcherInterface<SourceRange> {
public:
  IsSourceRangeMatcher(const ASTContext &Context, const Range &ExpectedRange);
  virtual bool MatchAndExplain(SourceRange ActualRange,
                               ::testing::MatchResultListener *Listener) const;
  virtual void DescribeTo(::std::ostream *os) const;
  virtual void DescribeNegationTo(::std::ostream *os) const;
  virtual ~IsSourceRangeMatcher() = default;

private:
  const ASTContext &Context;
  SourceRange ExpectedRange;
};

testing::Matcher<SourceRange> IsSourceRange(const ASTContext &Context,
                                            const Range &Range);
} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_MOCK_IS_SOURCE_RANGE_MATCHER_H
