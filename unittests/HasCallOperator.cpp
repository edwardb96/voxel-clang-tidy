#include "../../../../../unittests/ASTMatchers/ASTMatchersTest.h"
#include "../ASTMatchers.h"
#include <gtest/gtest.h>

namespace clang {
namespace tidy {
namespace voxel {
using namespace ast_matchers;

TEST(HasCallOperator, MatchesSimpleCase) {
  auto Matcher = lambdaExpr(hasCallOperator(functionDecl(anything())));
  EXPECT_TRUE(matches("auto f = [](){};", Matcher));
  EXPECT_TRUE(matches("auto f = []() -> int { return 0; };", Matcher));
  EXPECT_TRUE(matches("auto f = [](int x) -> int { return x; };", Matcher));
  EXPECT_TRUE(matches("int main() {"
                      "  int y = 10;"
                      "  auto f = [&y](int x) -> int { return x + y; };"
                      "  return 0;"
                      "}",
                      Matcher));
}

} // namespace voxel
} // namespace tidy
} // namespace clang
