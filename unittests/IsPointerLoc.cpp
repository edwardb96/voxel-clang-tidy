#include "../../../../../unittests/ASTMatchers/ASTMatchersTest.h"
#include "../ASTMatchers.h"
#include <gtest/gtest.h>

namespace clang {
namespace tidy {
namespace voxel {
using namespace ast_matchers;

TEST(IsPointerLoc, MatchesSimpleCase) {
  auto Matcher = typeLoc(isPointerLoc());
  EXPECT_TRUE(matches("int* x = nullptr;", Matcher));
}

TEST(IsPointerLoc, MatchesDeduced) {
  auto Matcher = typeLoc(isPointerLoc());
  EXPECT_TRUE(matches("auto x = 0;"
                      "auto* y = &x;",
                      Matcher));
}

TEST(IsPointerLoc, MatchesTemplateParam) {
  auto Matcher = typeLoc(isPointerLoc());
  EXPECT_TRUE(matches("template<typename T> void f(T* x) {}", Matcher));
}

} // namespace voxel
} // namespace tidy
} // namespace clang
