#include "../../../../../unittests/ASTMatchers/ASTMatchersTest.h"
#include "../ASTMatchers.h"
#include "VerifyNodePredicateTrue.h"
#include <gtest/gtest.h>

namespace clang {
namespace tidy {
namespace voxel {
using namespace ast_matchers;

TEST(IsTemplateSpecializationLoc, MatchesSimpleCase) {
  auto Matcher = typeLoc(isTemplateSpecializationLoc());
  EXPECT_TRUE(matches("template <typename> class Template;"
                      "using t = Template<int>;",
                      Matcher));
  EXPECT_TRUE(matches("template <typename, typename> class Template;"
                      "using t = Template<double, double>;",
                      Matcher));
}

TEST(IsTemplateSpecializationLoc, MatchesVariadic) {
  auto Matcher = typeLoc(isTemplateSpecializationLoc());
  EXPECT_TRUE(matches("template <typename...> class Variadic;"
                      "using t = Variadic<>;",
                      Matcher));
  EXPECT_TRUE(matches("template <typename...> class Variadic;"
                      "using t = Variadic<double, int, bool>;",
                      Matcher));
}

TEST(IsTemplateSpecializationLoc, MatchesMultipleTimesForSameTemplate) {
  auto Matcher = typeLoc(isTemplateSpecializationLoc()).bind("loc");
  EXPECT_TRUE(matchAndVerifyResultTrue(
      "template <typename> class Template;"
      "using t1 = Template<int>;"
      "using t2 = Template<int>;",
      Matcher, llvm::make_unique<VerifyIdIsBoundTo<TypeLoc>>("loc", 2)));
}

TEST(IsTemplateSpecializationLoc, HasCorrectNumArgs) {
  auto Matcher = typeLoc(isTemplateSpecializationLoc()).bind("loc");
  EXPECT_TRUE(matchAndVerifyResultTrue(
      "template <typename, typename> class Template {};"
      "Template<int, double> x;"
      "Template<int, double> y;",
      Matcher,
      verifyNodeIs<TypeLoc>(
          "loc", [](TypeLoc const &Loc, ASTContext const &Context) -> bool {
            auto RealLoc =
                Loc.getUnqualifiedLoc().getAs<TemplateSpecializationTypeLoc>();
            return RealLoc.getNumArgs() == 2;
          }, 2)));
}

TEST(IsTemplateSpecializationLoc, DoesNotMatchArrays) {
  auto Matcher = typeLoc(isTemplateSpecializationLoc());
  EXPECT_TRUE(notMatches("using t = char *const[];", Matcher));
}
} // namespace voxel
} // namespace tidy
} // namespace clang
