#include "CountingLexer.h"
namespace clang {
namespace tidy {
namespace voxel {
CountingSimpleLexer::CountingSimpleLexer(SimpleLexer RealLexer, int &CallCount)
    : RealLexer(std::move(RealLexer)), CallCount(CallCount) {}

llvm::Optional<Token> CountingSimpleLexer::nextToken() {
  ++CallCount;
  return RealLexer.nextToken();
}

CountingLexerFactory::CountingLexerFactory(const ASTContext &Context,
                                           int &CallCount)
    : Context(Context), CallCount(CallCount) {}

CountingSimpleLexer CountingLexerFactory::operator()(SourceLocation Loc) const {
  return CountingSimpleLexer(SimpleLexer(Loc, Context), CallCount);
}
} // namespace voxel
} // namespace tidy
} // namespace clang
