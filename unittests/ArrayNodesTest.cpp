#include "../DeclaredTypeNodes.h"
#include "../LinearSearchForConst.h"
#include "../SimpleLexer.h"
#include "IsSourceRangeMatcher.h"
#include "MockLinearSearchForConst.h"
#include "VerifyNodes.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>
namespace clang {
namespace tidy {
namespace voxel {
using ::testing::_;

class ArrayNodesTest : public ::testing::Test {
public:
  MockLinearSearchForConst const &mockSearcher(const ASTContext &Context) {
    MockSearch = llvm::make_unique<MockLinearSearchForConst>(Context);
    return *MockSearch;
  }

  testing::AssertionResult extractedFromSource(
      llvm::StringRef Code,
      std::unique_ptr<ast_matchers::BoundNodesCallback> Callback) {
    return matchAndVerifyResultTrue(
        Code, ast_matchers::fieldDeclOfTypeContainingConst(),
        std::move(Callback));
  }

  template <typename PerMatchCallback>
  std::unique_ptr<ast_matchers::VerifyBoundNodes> is(PerMatchCallback OnMatch,
                                                     int ExpectedCount = 1) {
    return ast_matchers::VerifyExtractedNodes(
        MakeExtractArrayNodes(ExtractDeclaredTypeNodes<FieldDecl>()), OnMatch,
        ExpectedCount);
  }

protected:
  std::unique_ptr<MockLinearSearchForConst> MockSearch;
};

TEST_F(ArrayNodesTest, ExtractsConstReturnType) {
  EXPECT_TRUE(extractedFromSource(
      "class F { int const f[]; };",
      ast_matchers::VerifyNodes([](ast_matchers::BoundNodes const &Nodes,
                                   ASTContext const &Context) {
        auto Extract =
            MakeExtractArrayNodes(ExtractDeclaredTypeNodes<FieldDecl>());
        auto ContextNodes = Extract(Nodes);
        EXPECT_TRUE(ContextNodes.hasValue());
        EXPECT_TRUE(HasBoundNodeEqualTo(TypeLocToFix(ContextNodes.getValue()),
                                        "loc", Nodes));
        EXPECT_TRUE(HasBoundNodeEqualTo(ContextNodes.getValue().ArrayLoc,
                                        "array", Nodes));
        EXPECT_TRUE(HasBoundNodeEqualTo(
            &LeftParentContext(ContextNodes.getValue()), "decl", Nodes));
      })));
}
/*
TEST_F(ArrayNodesTest, FindsRightConstInReturnTypeSimple) {
  EXPECT_TRUE(extractedFromSource(
      "class F { int const f[]; };",
      is([this](ArrayNodes<DeclaredTypeNodes<FieldDecl>> const &Nodes,
                ASTContext const &Context) {
        auto MaybeBadConst =
            FindRightConst(tokenCountingSearcher(Context), Nodes);
        ASSERT_TRUE(MaybeBadConst.hasValue());
        auto BadConst = MaybeBadConst.getValue();
        EXPECT_EQ(Loc(Line(1), Column(5)).in(Context), BadConst.getLocation());
      })));
}

TEST_F(ArrayNodesTest, FindsNoRightConstWhenOnLeft) {
  EXPECT_TRUE(extractedFromSource(
      "const int f(){ return 0; }",
      is([this](FunctionReturnTypeNodes const &Nodes,
                ASTContext const &Context) {
        auto MaybeBadConst =
            FindRightConst(tokenCountingSearcher(Context), Nodes);
        EXPECT_FALSE(MaybeBadConst.hasValue());
      })));
}
*/

TEST_F(ArrayNodesTest, FindRightConstPerformsCorrectTraversal) {
  EXPECT_TRUE(extractedFromSource(
      "class F { int const f[]; };",
      is([this](ArrayNodes<DeclaredTypeNodes<FieldDecl>> const &Nodes,
                ASTContext const &Context) {
        auto &SearchForConst = mockSearcher(Context);
        EXPECT_CALL(SearchForConst,
                    LeftToRight(IsSourceRange(
                        Context, Range(Loc(Line(1), Column(14)),
                                       Loc(Line(1), Column(22))))));
        FindRightConst(SearchForConst, Nodes);
      })));
}

} // namespace voxel
} // namespace tidy
} // namespace clang
