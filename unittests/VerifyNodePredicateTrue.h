//===- unittest/Tooling/ASTMatchersTest.h - Matcher tests helpers ------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#include "../../../../../unittests/ASTMatchers/ASTMatchersTest.h"

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_VERIFY_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_VERIFY_H
namespace clang {
namespace ast_matchers {

template <typename T, typename Predicate>
class VerifyIdIsBoundToNodeWhich : public BoundNodesCallback {
public:
  explicit VerifyIdIsBoundToNodeWhich(llvm::StringRef Id, Predicate NodeMatches)
      : Id(Id), ExpectedCount(-1), Count(0), NodeMatches(NodeMatches) {}

  VerifyIdIsBoundToNodeWhich(llvm::StringRef Id, Predicate NodeMatches,
                             int ExpectedCount)
      : Id(Id), ExpectedCount(ExpectedCount), Count(0),
        NodeMatches(NodeMatches) {}

  void onEndOfTranslationUnit() override {
    if (ExpectedCount != -1) {
      EXPECT_EQ(ExpectedCount, Count);
    }
    Count = 0;
  }

  ~VerifyIdIsBoundToNodeWhich() override { EXPECT_EQ(0, Count); }

  bool run(const BoundNodes *Nodes, ASTContext *Context) override {
    const BoundNodes::IDToNodeMap &M = Nodes->getMap();
    if (auto *Node = Nodes->getNodeAs<T>(Id)) {
      ++Count;
      BoundNodes::IDToNodeMap::const_iterator I = M.find(Id);
      EXPECT_NE(M.end(), I);
      if (I != M.end()) {
        EXPECT_EQ(Node, I->second.get<T>());
      }
      EXPECT_TRUE(NodeMatches(*Node, *Context));
      return true;
    }
    EXPECT_TRUE(M.count(Id) == 0 ||
                M.find(Id)->second.template get<T>() == nullptr);
    return false;
  }

  bool run(const BoundNodes *Nodes) override {
    llvm::errs() << "Calling VerifyIdIsBoundToNodeWhich without Context is not "
                    "supported.\n";
    return false;
  }

private:
  const std::string Id;
  const int ExpectedCount;
  int Count;
  Predicate NodeMatches;
};

template <typename T, typename Predicate>
std::unique_ptr<VerifyIdIsBoundToNodeWhich<T, Predicate>>
verifyNodeIs(llvm::StringRef Id, Predicate NodeMatches, int ExpectedCount = 1) {
  return llvm::make_unique<VerifyIdIsBoundToNodeWhich<T, Predicate>>(
      Id, NodeMatches, ExpectedCount);
}

} // namespace ast_matchers
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_VERIFY_H
