#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_MOCK_LAZY_SOURCE_LOCATIONS_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_MOCK_LAZY_SOURCE_LOCATIONS_H
#include "clang/AST/ASTContext.h"
#include "clang/Basic/SourceLocation.h"
namespace clang {
namespace tidy {
namespace voxel {
struct Line {
  explicit Line(int LineNumber) : LineNumber(LineNumber) {}
  const int LineNumber;
};

struct Column {
  explicit Column(int Column) : ColumnNumber(Column) {}
  const int ColumnNumber;
};

SourceLocation FileLineColToSourceLocation(const ASTContext &Context, Line L,
                                           Column C);
class Loc {
public:
  Loc(Line L, Column C) : L(L), C(C) {}

  SourceLocation in(const ASTContext &Context) const {
    return FileLineColToSourceLocation(Context, L, C);
  }

private:
  Line L;
  Column C;
};

class Range {
public:
  Range(Loc Begin, Loc End) : Begin(Begin), End(End) {}

  SourceRange in(const ASTContext &Context) const {
    return SourceRange(Begin.in(Context), End.in(Context));
  }

private:
  Loc Begin;
  Loc End;
};

} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_MOCK_LAZY_SOURCE_LOCATIONS_H
