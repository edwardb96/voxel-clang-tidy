#include "ConstPositionCheck.h"
#include "ASTMatchers.h"
#include "ConstPositionChecker.h"
#include "ConstPositionCorrector.h"
#include "DeclaredTypeNodes.h"
#include "GetEndLoc.h"
#include "MisplacedConstDiagnosticGenerator.h"
#include "PointerNodes.h"
#include "ReturnTypeNodes.h"
#include "SimpleLexer.h"
#include "TemplateArgumentNodes.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/ExprCXX.h"
#include "clang/AST/Type.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "llvm/ADT/STLExtras.h"
#include <cassert>

using namespace clang::ast_matchers;

namespace clang {
namespace tidy {
namespace voxel {
ConstPositionCheck::ConstPositionCheck(StringRef Name,
                                       ClangTidyContext *Context)
    : ClangTidyCheck(Name, Context),
      PreferredConstPosition(
          ConstPositionFromString(Options.get("ConstPosition", "Right"))
              .getValue()) {}

void ConstPositionCheck::registerMatchers(MatchFinder *Finder) {
  Finder->addMatcher(varDeclOfTypeContainingConst(), this);
  Finder->addMatcher(fieldDeclOfTypeContainingConst(), this);
  Finder->addMatcher(usingDeclOfTypeContainingConst(), this);
  Finder->addMatcher(typedefDeclOfTypeContainingConst(), this);
  Finder->addMatcher(functionDeclReturnTypeContainingConst(), this);
  Finder->addMatcher(lambdaExprReturnTypeContainingConst(), this);
  Finder->addMatcher(classTemplateArgumentContainingConst(), this);
  Finder->addMatcher(functionTemplateArgumentContainingConst(), this);
}

void ConstPositionCheck::storeOptions(ClangTidyOptions::OptionMap &Opts) {
  Options.store(Opts, "ConstPosition", "Left");
}

template <ConstPosition PreferredPosition>
void ConstPositionCheck::checkConstIsOnThe(
    const MatchFinder::MatchResult &Result) {
  auto const &Context = *Result.Context;

  // auto *Loc = Result.Nodes.getNodeAs<TypeLoc>("loc");
  // if (Loc != nullptr) {
  //  llvm::outs() << "Matched At\n";
  //  Loc->getBeginLoc().print(llvm::outs(), Context.getSourceManager());
  //  llvm::outs() << " to\n";
  //  EndCharLoc(Context, *Loc).print(llvm::outs(), Context.getSourceManager());
  //  llvm::outs() << "\n";
  //}

  auto CreateDiagnostic =
      MisplacedConstDiagnosticGenerator<PreferredPosition>(*this, Context);

  auto LinearSearchForConst = MakeLinearSearcherForConst(
      [&Context](SourceLocation Loc) -> SimpleLexer {
        return SimpleLexer(Loc, Context);
      },
      Context);

  auto FindBadConst =
      MakeBadConstFinder<Opposite(PreferredPosition)>(LinearSearchForConst);

  auto Checker = MakeConstPositionChecker(
      FindBadConst, ConstPositionCorrector<PreferredPosition>(Context),
      CreateDiagnostic);

  auto const &Nodes = Result.Nodes;
  auto DeducedMatch =
      checkInAllTypeContexts(Checker, ExtractDeclaredTypeNodes<VarDecl>(),
                             Nodes) ||
      Checker.check(MakeExtractArrayNodes(ExtractDeclaredTypeNodes<VarDecl>()),
                    Nodes) ||
      Checker.check(MakeExtractArrayNodes(ExtractDeclaredTypeNodes<FieldDecl>()),
                    Nodes) ||
      checkInAllTypeContexts(Checker, ExtractDeclaredTypeNodes<FieldDecl>(),
                             Nodes) ||
      checkInAllTypeContexts(Checker, ExtractDeclaredTypeNodes<TypeAliasDecl>(),
                             Nodes) ||
      checkInAllTypeContexts(Checker, ExtractDeclaredTypeNodes<TypedefDecl>(),
                             Nodes) ||
      checkInAllTypeContexts(Checker, ExtractFunctionReturnTypeNodes(),
                             Nodes) ||
      checkInAllTypeContexts(Checker, ExtractLambdaReturnTypeNodes(), Nodes) ||
      checkInAllTypeContexts(Checker, ExtractClassTemplateArgumentNodes(),
                             Nodes) ||
      Checker.check(ExtractFunctionTemplateArgumentNodes(), Nodes);

  if (!DeducedMatch) {
    llvm::outs() << "error: No Extractors Matched\n";
    for (auto &&Node : Nodes.getMap()) {
      llvm::outs() << Node.first << " : ";
      Node.second.print(llvm::outs(), PrintingPolicy(getLangOpts()));
      llvm::outs() << "\n";
    }
  }
}

template <typename Check, typename SimpleExtractor>
bool ConstPositionCheck::checkInAllTypeContexts(
    Check &Checker, SimpleExtractor const &Extractor,
    const ast_matchers::BoundNodes &Nodes) {
  return Checker.check(MakeExtractPointerNodes(Extractor), Nodes) ||
         //Checker.check(MakeExtractArrayNodes(Extractor), Nodes) ||
         Checker.check(Extractor, Nodes);
}

void ConstPositionCheck::check(const MatchFinder::MatchResult &Result) {
  switch (PreferredConstPosition) {
  case ConstPosition::Left:
    checkConstIsOnThe<ConstPosition::Left>(Result);
    break;
  case ConstPosition::Right:
    checkConstIsOnThe<ConstPosition::Right>(Result);
    break;
  }
}

} // namespace voxel
} // namespace tidy
} // namespace clang
