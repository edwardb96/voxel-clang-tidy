//===--- ASTMatchers.h - clang-tidy------------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_VOXEL_ASTMATCHERS_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_VOXEL_ASTMATCHERS_H
#include "ASTMatchersInternal.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchersMacros.h"
namespace clang {
namespace ast_matchers {

AST_MATCHER(QualType, isLocallyConstQualified) {
  return internal::IsLocallyConstQualified(Node);
}

AST_MATCHER(TypeLoc, isTemplateSpecializationLoc) {
  return internal::UnqualifiedCanCastTo<TemplateSpecializationTypeLoc>(Node);
}

AST_MATCHER(TypeLoc, isPointerLoc) {
  return internal::UnqualifiedCanCastTo<PointerTypeLoc>(Node);
}

AST_MATCHER(TypeLoc, isReferenceLoc) {
  return internal::UnqualifiedCanCastTo<ReferenceTypeLoc>(Node);
}

AST_MATCHER(TypeLoc, isArrayLoc) {
  return internal::UnqualifiedCanCastTo<ArrayTypeLoc>(Node);
}

AST_MATCHER_P(LambdaExpr, hasCallOperator, internal::Matcher<CXXMethodDecl>,
              InnerMatcher) {
  if (auto *CallOperator = Node.getCallOperator())
    if (InnerMatcher.matches(*CallOperator, Finder, Builder))
      return true;
    else
      return false;
  else
    return false;
}

AST_MATCHER_P(FunctionDecl, returnTypeLoc, internal::Matcher<TypeLoc>,
              InnerMatcher) {
  auto ReturnTypeLoc = internal::ReturnTypeLoc(Node);
  if (ReturnTypeLoc.hasValue())
    return InnerMatcher.matches(ReturnTypeLoc.getValue(), Finder, Builder);
  else
    return false;
}

AST_POLYMORPHIC_MATCHER_P(
    declaredType,
    AST_POLYMORPHIC_SUPPORTED_TYPES(DeclaratorDecl, TypeAliasDecl, TypedefDecl),
    ast_matchers::internal::Matcher<TypeLoc>, InnerMatcher) {
  auto DeclaredTypeLoc = internal::DeclaredTypeLoc(Node);
  if (DeclaredTypeLoc.hasValue())
    return InnerMatcher.matches(DeclaredTypeLoc.getValue(), Finder, Builder);
  else
    return false;
}

AST_MATCHER(DeclRefExpr, hasExplicitTemplateArgs) {
  return Node.hasExplicitTemplateArgs();
}

inline internal::Matcher<TypeLoc> isConstNonPointerLoc() {
  return allOf(loc(qualType(isLocallyConstQualified())).bind("loc"),
               unless(isPointerLoc()));
}

inline internal::Matcher<TypeLoc> findConstRefereeLocs() {
  return typeLoc(isReferenceLoc(),
                 has(typeLoc(isConstNonPointerLoc()).bind("loc")));
}

} // namespace ast_matchers
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_VOXEL_ASTMATCHERS_H
