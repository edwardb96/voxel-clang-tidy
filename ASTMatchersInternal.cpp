#include "ASTMatchersInternal.h"
namespace clang {
namespace ast_matchers {
namespace internal {

llvm::Optional<TypeLoc> TypeLocIfTypeArg(const TemplateArgumentLoc &Node) {
  const TypeSourceInfo *TSI = Node.getTypeSourceInfo();
  if (TSI != nullptr) {
    auto Loc = TSI->getTypeLoc().IgnoreParens();
    if (Loc)
      return Loc;
    else
      return llvm::None;
  } else {
    return llvm::None;
  }
}

llvm::Optional<TypeLoc> ReturnTypeLoc(const FunctionDecl &Node) {
  const TypeSourceInfo *TSI = Node.getTypeSourceInfo();
  if (TSI != nullptr) {
    auto FunctionType =
        TSI->getTypeLoc().IgnoreParens().getAs<FunctionTypeLoc>();
    if (FunctionType) {
      auto ReturnType = FunctionType.getReturnLoc();
      if (ReturnType) {
        return ReturnType;
      } else {
        return llvm::None;
      }
    } else {
      return llvm::None;
    }
  } else {
    return llvm::None;
  }
}

bool IsLocallyConstQualified(const QualType &Type) {
  return Type.isLocalConstQualified();
}
} // namespace internal
} // namespace ast_matchers
} // namespace clang
