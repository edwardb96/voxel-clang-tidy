//===--- ConstPositionCheck.h - clang-tidy--------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_CONST_POSITION_CHECK_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_CONST_POSITION_CHECK_H

#include "../ClangTidy.h"
#include "BadConstFinder.h"
#include "ConstPosition.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/StringRef.h"
#include <string>
#include <type_traits>

using namespace clang::ast_matchers;

namespace clang {
namespace tidy {
namespace voxel {

template <typename AstNode>
using IsDerivedFromTypeLoc = std::is_base_of<TypeLoc, AstNode>;

/// Checks that the const qualifier is on the preferred side of the type.
///
/// For the user-facing documentation see:
/// http://clang.llvm.org/extra/clang-tidy/checks/const-position-check.html
class ConstPositionCheck : public ClangTidyCheck {
private:
  ConstPosition PreferredConstPosition;

  template <ConstPosition PreferredConstPosition>
  void checkConstIsOnThe(const MatchFinder::MatchResult &Result);

  template <typename Check, typename SimpleExtractor>
  bool checkInAllTypeContexts(Check &Checker, const SimpleExtractor &Extractor,
                              const ast_matchers::BoundNodes &BoundNodes);

  const TypeLoc *getTypeLoc(const MatchFinder::MatchResult &Result) const;

public:
  ConstPositionCheck(StringRef Name, ClangTidyContext *Context);
  void registerMatchers(ast_matchers::MatchFinder *Finder) override;
  void check(const ast_matchers::MatchFinder::MatchResult &Result) override;
  void storeOptions(ClangTidyOptions::OptionMap &Opts) override;
};

} // namespace voxel
} // namespace tidy
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_CONST_POSITION_CHECK_H
