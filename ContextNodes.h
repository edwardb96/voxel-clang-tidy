#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_CONTEXT_NODES_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_CONTEXT_NODES_H
#include "LinearSearchForConst.h"
#include "TokenPredicates.h"
#include "llvm/ADT/Optional.h"

namespace clang {
namespace tidy {
namespace voxel {

template <typename ContextNodes>
auto LeftParentContext(ContextNodes const &Nodes)
    -> decltype(ParentContext(Nodes)) {
  return ParentContext(Nodes);
}

template <typename ContextNodes>
auto RightParentContext(ContextNodes const &Nodes)
    -> decltype(ParentContext(Nodes)) {
  return ParentContext(Nodes);
}

template <typename ContextNodes>
auto LeftQualifierSpace(ContextNodes const &Nodes)
    -> decltype(LeftQualifierSpace(TypeLocToFix(Nodes), LeftParentContext(Nodes))) {
  return LeftQualifierSpace(TypeLocToFix(Nodes), LeftParentContext(Nodes));
}

template <typename ContextNodes>
auto RightQualifierSpace(ContextNodes const &Nodes)
    -> decltype(RightQualifierSpace(TypeLocToFix(Nodes), RightParentContext(Nodes))) {
  return RightQualifierSpace(TypeLocToFix(Nodes), RightParentContext(Nodes));
}

} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_CONTEXT_NODES_H
