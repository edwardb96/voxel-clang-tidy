//===--- PointerNodes.h - clang-tidy----------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_POINTER_NODES_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_POINTER_NODES_H
#include "ExtractorBase.h"
#include "GetEndLoc.h"
#include "LinearSearchForConst.h"
#include "MakeIfAllHaveValues.h"
#include "clang/AST/TypeLoc.h"
#include "llvm/ADT/Optional.h"
#include <type_traits>

namespace clang {
namespace ast_matchers {
ast_matchers::internal::Matcher<TypeLoc> findAllNonPointerConstPointeeLocs();
}
namespace tidy {
namespace voxel {
template <typename PointeeContext> struct PointerNodes {
  PointerNodes(PointeeContext Pointee, PointerTypeLoc PointerLoc)
      : Pointee(std::move(Pointee)), PointerLoc(PointerLoc) {}

  PointeeContext Pointee;
  PointerTypeLoc PointerLoc;
};

template <typename PointeeContext, typename PointeeContextExtractor>
class ExtractPointerNodes {
public:
  ExtractPointerNodes(PointeeContextExtractor const &ExtractPointeeNodes)
      : ExtractPointeeNodes(ExtractPointeeNodes) {}

  llvm::Optional<PointerNodes<PointeeContext>>
  operator()(ast_matchers::BoundNodes const &BoundNodes) {
    return MakeIfAllHaveValues<PointerNodes<PointeeContext>>(
        ExtractPointeeNodes(BoundNodes),
        Extract<PointerTypeLoc>(BoundNodes, "raw-pointer"));
  }

private:
  PointeeContextExtractor ExtractPointeeNodes;
};

template <typename PointeeContextExtractor,
          typename PointeeContext = ResultOfExtractor<PointeeContextExtractor>>
ExtractPointerNodes<PointeeContext, PointeeContextExtractor>
MakeExtractPointerNodes(PointeeContextExtractor const &ExtractPointeeNodes) {
  return ExtractPointerNodes<PointeeContext, PointeeContextExtractor>(
      ExtractPointeeNodes);
}

template <typename PointeeContext>
const TypeLoc &TypeLocToFix(PointerNodes<PointeeContext> const &Nodes) {
  return TypeLocToFix(Nodes.Pointee);
}

template <typename PointeeContext>
auto LeftParentContext(PointerNodes<PointeeContext> const &Nodes)
    -> decltype(ParentContext(Nodes.Pointee)) {
  return LeftParentContext(Nodes.Pointee);
}

template <typename PointeeContext>
const PointerTypeLoc &
RightParentContext(PointerNodes<PointeeContext> const &Nodes) {
  return Nodes.Pointer;
}

inline LeftToRightSourceTraversal<RightOfNode<TypeLoc>, SourceLocation>
RightQualifierSpace(TypeLoc PointeeLoc, PointerTypeLoc const &Ancestor) {
  return LeftToRight(RightOf(PointeeLoc), Ancestor.getStarLoc());
}

template <typename PointeeContext, typename LinearSearcher>
llvm::Optional<Token>
FindRightConst(LinearSearcher const &LinearSearchForConst,
               PointerNodes<PointeeContext> const &Nodes) {
  return LinearSearchForConst(
      RightQualifierSpace(TypeLocToFix(Nodes.Pointee), Nodes.PointerLoc));
}
} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_POINTER_NODES_H
