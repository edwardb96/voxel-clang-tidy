//===--- SourceTraversal.h - clang-tidy---------------------------*- C++
//-*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_SOURCE_TRAVERSAL_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_SOURCE_TRAVERSAL_H
#include "SourceLocations.h"
#include "clang/AST/ASTContext.h"

namespace clang {
namespace tidy {
namespace voxel {
template <typename SourceLocationRequiringContext>
SourceLocation GetSourceLocation(SourceLocationRequiringContext const &Position,
                                 ASTContext const &Context) {
  return Position.getSourceLocation(Context);
}

inline SourceLocation GetSourceLocation(SourceLocation const &Location,
                                        ASTContext const &) {
  return Location;
}

template <typename StartLocation, typename EndLocation>
class BoundedSourceTraversal {
public:
  BoundedSourceTraversal(StartLocation Start, EndLocation End)
      : Start(Start), End(End) {}

protected:
  SourceLocation startSourceLocation(ASTContext const &Context) const {
    return GetSourceLocation(Start, Context);
  }

  SourceLocation endSourceLocation(ASTContext const &Context) const {
    return GetSourceLocation(End, Context);
  }

private:
  StartLocation Start;
  EndLocation End;
};

template <typename StartLocation, typename EndLocation>
class LeftToRightSourceTraversal
    : public BoundedSourceTraversal<StartLocation, EndLocation> {
public:
  using BoundedSourceTraversal<StartLocation,
                               EndLocation>::BoundedSourceTraversal;
  SourceRange range(ASTContext const &Context) const {
    return SourceRange(this->startSourceLocation(Context),
                       this->endSourceLocation(Context));
  }
};

template <typename StartLocation, typename EndLocation>
LeftToRightSourceTraversal<StartLocation, EndLocation>
LeftToRight(StartLocation Start, EndLocation End) {
  return LeftToRightSourceTraversal<StartLocation, EndLocation>(Start, End);
}

template <typename StartLocation, typename EndLocation>
class RightToLeftSourceTraversal
    : public BoundedSourceTraversal<StartLocation, EndLocation> {
public:
  using BoundedSourceTraversal<StartLocation,
                               EndLocation>::BoundedSourceTraversal;
  SourceRange range(ASTContext const &Context) {
    return SourceRange(this->endSourceLocation(Context),
                       this->startSourceLocation(Context));
  }
};

template <typename StartLocation, typename EndLocation>
RightToLeftSourceTraversal<StartLocation, EndLocation>
RightToLeft(StartLocation Start, EndLocation End) {
  return RightToLeftSourceTraversal<StartLocation, EndLocation>(Start, End);
}

template <typename StartLocation> class UnboundedSourceTraversal {
public:
  UnboundedSourceTraversal(StartLocation Start) : Start(Start) {}
  SourceLocation start(ASTContext const &Context) const {
    return GetSourceLocation(Start, Context);
  }

private:
  StartLocation Start;
};

template <typename StartLocation>
class UnboundedLeftToRightSourceTraversal
    : public UnboundedSourceTraversal<StartLocation> {
  using UnboundedSourceTraversal<StartLocation>::UnboundedSourceTraversal;
};

template <typename StartLocation>
UnboundedLeftToRightSourceTraversal<StartLocation>
LeftToRight(StartLocation Start) {
  return UnboundedLeftToRightSourceTraversal<StartLocation>(Start);
}

} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_SOURCE_TRAVERSAL_H
