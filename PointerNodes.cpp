#include "PointerNodes.h"
#include "ASTMatchers.h"

namespace clang {
namespace ast_matchers {
ast_matchers::internal::Matcher<TypeLoc> findAllNonPointerConstPointeeLocs() {
  return findAll(typeLoc(isPointerLoc(), // Could be in a template param.
                         forEach(typeLoc(isConstNonPointerLoc()).bind("loc")))
                     .bind("raw-pointer"));
}
} // namespace ast_matchers
} // namespace clang
