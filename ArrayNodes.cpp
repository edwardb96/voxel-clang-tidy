#include "ArrayNodes.h"
#include "ASTMatchers.h"
namespace clang {
namespace ast_matchers {
internal::Matcher<TypeLoc> findAllNonPointerConstArrayLocs() {
  return findAll(typeLoc(isArrayLoc(),
                         forEach(typeLoc(isConstNonPointerLoc()).bind("loc")))
                     .bind("array"));
}
} // namespace ast_matchers
} // namespace clang
