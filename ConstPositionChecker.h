#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_CONST_POSITION_CHECKER_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_CONST_POSITION_CHECKER_H
#include "clang/AST/TypeLoc.h"
#include "clang/Lex/Token.h"
#include <cassert>
namespace clang {
namespace tidy {
namespace voxel {

template <typename Finder, typename Corrector, typename DiagnosticGenerator>
class ConstPositionChecker {
public:
  ConstPositionChecker(Finder FindBadConst, Corrector CorrectConstPosition,
                       DiagnosticGenerator ApplyDiagnostic)
      : FindBadConst(FindBadConst), CorrectConstPosition(CorrectConstPosition),
        ApplyDiagnostic(ApplyDiagnostic) {}

  template <typename Extractor>
  bool check(Extractor ExtractUsefulNodes, const ast_matchers::BoundNodes& BoundNodes) {
    auto MaybeNodes = ExtractUsefulNodes(BoundNodes);
    if (MaybeNodes.hasValue()) {
      auto const &Nodes = MaybeNodes.getValue();
      auto BadConst = FindBadConst(Nodes);
      if (BadConst.hasValue()) {
        auto const &TypeLocation = TypeLocToFix(Nodes);
        auto GoodConstLocation = CorrectConstPosition(TypeLocation);
        ApplyDiagnostic(BadConst.getValue(), GoodConstLocation, TypeLocation);
      }
      return true;
    } else {
      return false;
    }
  }

private:
  Finder FindBadConst;
  Corrector CorrectConstPosition;
  DiagnosticGenerator ApplyDiagnostic;
};

template <typename Finder, typename Corrector, typename DiagnosticGenerator>
ConstPositionChecker<Finder, Corrector, DiagnosticGenerator>
MakeConstPositionChecker(Finder FindBadConst, Corrector PositionCorrector,
                         DiagnosticGenerator ApplyDiagnostic) {
  return ConstPositionChecker<Finder, Corrector, DiagnosticGenerator>(
      FindBadConst, PositionCorrector, ApplyDiagnostic);
}

} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_CONST_POSITION_CHECKER_H
