//===--- GenericVariableNamesCheck.h - clang-tidy--------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_NO_GENERIC_VARIABLE_NAMES_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_NO_GENERIC_VARIABLE_NAMES_H

#include "../ClangTidy.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/StringRef.h"

namespace clang {
namespace tidy {
namespace voxel {

/// FIXME: Write a short description.
///
/// For the user-facing documentation see:
/// http://clang.llvm.org/extra/clang-tidy/checks/readability-no-generic-variable-names.html
class GenericVariableNamesCheck : public ClangTidyCheck {
public:
  GenericVariableNamesCheck(StringRef Name, ClangTidyContext *Context)
      : ClangTidyCheck(Name, Context) {}
  void registerMatchers(ast_matchers::MatchFinder *Finder) override;
  void check(const ast_matchers::MatchFinder::MatchResult &Result) override;
  llvm::Optional<StringRef> nameIfFieldDecl(const ast_matchers::MatchFinder::MatchResult &Result);
  llvm::Optional<StringRef> nameIfVariableDecl(const ast_matchers::MatchFinder::MatchResult &Result);
};

} // namespace readability
} // namespace tidy
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_NO_GENERIC_VARIABLE_NAMES_H
