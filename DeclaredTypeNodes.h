//===--- PointerNodes.h - clang-tidy----------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_DECLARED_TYPE_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_DECLARED_TYPE_H
#include "ArrayNodes.h"
#include "ContextNodes.h"
#include "ExtractorBase.h"
#include "GetEndLoc.h"
#include "MakeIfAllHaveValues.h"
#include "PointerNodes.h"
#include "SourceLocations.h"
#include "clang/AST/TypeLoc.h"
#include "llvm/ADT/Optional.h"

namespace clang {
namespace ast_matchers {
internal::Matcher<Decl> varDeclOfTypeContainingConst();
internal::Matcher<Decl> fieldDeclOfTypeContainingConst();
internal::Matcher<Decl> usingDeclOfTypeContainingConst();
internal::Matcher<Decl> typedefDeclOfTypeContainingConst();
} // namespace ast_matchers
namespace tidy {
namespace voxel {
template <typename DeclNode> struct DeclaredTypeNodes {
  DeclaredTypeNodes(NodeStorage<DeclNode> Decl, TypeLoc Loc)
      : Decl(Decl), Loc(Loc) {}

  NodeStorage<DeclNode> Decl;
  TypeLoc Loc;
};

template <typename DeclNode> class ExtractDeclaredTypeNodes {
public:
  llvm::Optional<DeclaredTypeNodes<DeclNode>>
  operator()(const ast_matchers::BoundNodes &BoundNodes) const {
    return MakeIfAllHaveValues<DeclaredTypeNodes<DeclNode>>(
        Extract<DeclNode>(BoundNodes, "decl"),
        Extract<TypeLoc>(BoundNodes, "loc"));
  }
};

template <typename DeclNode>
const TypeLoc &TypeLocToFix(DeclaredTypeNodes<DeclNode> const &Nodes) {
  return Nodes.Loc;
}

template <typename DeclNode>
const DeclNode &ParentContext(DeclaredTypeNodes<DeclNode> const &Nodes) {
  return get(Nodes.Decl);
}

template <typename LinearSearcher, typename DeclNode>
llvm::Optional<Token> FindLeftConst(const LinearSearcher &LinearSearchForConst,
                                    DeclaredTypeNodes<DeclNode> const &Nodes) {
  return LinearSearchForConst(LeftQualifierSpace(Nodes));
}

template <typename LinearSearcher, typename DeclNode>
llvm::Optional<Token> FindRightConst(const LinearSearcher &LinearSearchForConst,
                                     DeclaredTypeNodes<DeclNode> const &Nodes) {
  return LinearSearchForConst(RightQualifierSpace(Nodes));
}

template <typename LinearSearcher, typename DeclNode>
llvm::Optional<Token>
FindLeftConst(const LinearSearcher &LinearSearchForConst,
              const PointerNodes<DeclaredTypeNodes<DeclNode>> &Nodes) {
  return LinearSearchForConst(LeftQualifierSpace(Nodes),
                              StoppingIf(&IsLeftTypeEdge));
}

template <typename LinearSearcher>
llvm::Optional<Token>
FindLeftConst(const LinearSearcher &LinearSearchForConst,
              const ArrayNodes<DeclaredTypeNodes<VarDecl>> &Nodes) {
  return LinearSearchForConst(LeftQualifierSpace(Nodes),
                              StoppingIf(&IsLeftTypeEdge));
}

template <typename LinearSearcher>
llvm::Optional<Token>
FindLeftConst(const LinearSearcher &LinearSearchForConst,
              const ArrayNodes<DeclaredTypeNodes<FieldDecl>> &Nodes) {
  return LinearSearchForConst(LeftQualifierSpace(Nodes),
                              StoppingIf(&IsLeftTypeEdge));
}

template <typename LinearSearcher>
llvm::Optional<Token>
FindLeftConst(const LinearSearcher &LinearSearchForConst,
              const DeclaredTypeNodes<TypeAliasDecl> &Nodes) {
  return LinearSearchForConst(
      LeftQualifierSpace(Nodes),
      StoppingIf([](Token Tok) -> bool { return Tok.is(tok::equal); }));
}

template <typename LinearSearcher>
llvm::Optional<Token>
FindRightConst(const LinearSearcher &LinearSearchForConst,
               const DeclaredTypeNodes<TypeAliasDecl> &Nodes) {
  return LinearSearchForConst(
      LeftToRight(RightOf(TypeLocToFix(Nodes))),
      StoppingIf([](Token Tok) -> bool { return Tok.is(tok::semi); }));
}

template <typename LinearSearcher>
llvm::Optional<Token>
FindLeftConst(const LinearSearcher &LinearSearchForConst,
              const PointerNodes<DeclaredTypeNodes<TypeAliasDecl>> &Nodes) {
  return LinearSearchForConst(
      LeftQualifierSpace(Nodes), StoppingIf([](Token Tok) -> bool {
        return IsLeftTypeEdge(Tok) || Tok.is(tok::equal);
      }));
}

class BeforeInit {
public:
  BeforeInit(VarDecl const &VarNode);
  SourceLocation getSourceLocation(ASTContext const &Context) const;

private:
  VarDecl const &VarNode;
};

LeftToRightSourceTraversal<RightOfNode<TypeLoc>, BeforeInit>
RightQualifierSpace(TypeLoc ArgumentLoc, VarDecl const &Ancestor);

class BeforeInClassInit {
public:
  BeforeInClassInit(FieldDecl const &FieldNode);
  SourceLocation getSourceLocation(ASTContext const &Context) const;

private:
  FieldDecl const &FieldNode;
};

LeftToRightSourceTraversal<RightOfNode<TypeLoc>, BeforeInClassInit>
RightQualifierSpace(TypeLoc ArgumentLoc, FieldDecl const &Ancestor);

} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_DECLARED_TYPE_H
