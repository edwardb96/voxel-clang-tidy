#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_PRINT_TOKENS_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_PRINT_TOKENS_H
#include "clang/Lex/Token.h"
#include "clang/Lex/Token.h"
#include "llvm/Support/raw_ostream.h"
#include "clang/Basic/IdentifierTable.h"
namespace clang {
namespace tidy {
namespace voxel {
namespace {
inline void PrintToken(Token Token) {
//  llvm::outs() << tok::getTokenName(Token.getKind());
//  if (Token.is(tok::identifier)) {
//    IdentifierInfo &Info = *Token.getIdentifierInfo();
//    llvm::outs() << " '" << Info.getNameStart() << "'";
//  }
//  llvm::outs() << '\n';
}

inline void PrintTokens(const SmallVectorImpl<Token> &Tokens) {
  for (const auto &Token : Tokens)
    PrintToken(Token);
}
} // namespace
} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_PRINT_TOKENS_H
