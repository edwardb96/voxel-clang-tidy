#include "GetEndLoc.h"
#include "clang/Lex/Lexer.h"

namespace clang {
namespace tidy {
namespace voxel {

SourceLocation OnePastEndOfTokenAt(const ASTContext &Context,
                                   SourceLocation TokenLoc) {
  return Lexer::getLocForEndOfToken(TokenLoc, 0, Context.getSourceManager(),
                                    Context.getLangOpts());
}

SourceLocation EndCharLoc(const ASTContext &Context, const TypeLoc &Node) {
  // Getting one past the end on a doubly nested template specialization returns
  // two past the end.
  if (auto NodeAsTemplate = Node.getAs<TemplateSpecializationTypeLoc>())
    return Node.getLocEnd().getLocWithOffset(1);
  else
    return OnePastEndOfTokenAt(Context, Node.getLocEnd());
}

} // namespace voxel
} // namespace tidy
} // namespace clang
