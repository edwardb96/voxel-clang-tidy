//===--- BadConstFinder.h - clang-tidy---------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_BAD_CONST_FINDER_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_BAD_CONST_FINDER_H
#include "ConstPosition.h"
#include "ContextNodes.h"
#include "clang/AST/ASTContext.h"
#include "clang/Basic/LLVM.h"
#include "clang/Lex/Token.h"
#include "llvm/ADT/Optional.h"

namespace clang {
namespace tidy {
namespace voxel {

template <ConstPosition BadConstPosition, typename LinearSearcher>
class BadConstFinder;

template <typename LinearSearcher>
class BadConstFinder<ConstPosition::Left, LinearSearcher> {
public:
  explicit BadConstFinder(const LinearSearcher &LinearSearchForConst)
      : LinearSearchForConst(LinearSearchForConst) {}

  template <typename ContextNodes>
  llvm::Optional<Token> operator()(ContextNodes const &Nodes) {
    return FindLeftConst(LinearSearchForConst, Nodes);
  }

private:
  LinearSearcher LinearSearchForConst;
};

template <typename LinearSearcher>
class BadConstFinder<ConstPosition::Right, LinearSearcher> {
public:
  explicit BadConstFinder(const LinearSearcher &LinearSearchForConst)
      : LinearSearchForConst(LinearSearchForConst) {}

  template <typename ContextNodes>
  llvm::Optional<Token> operator()(ContextNodes const &Nodes) {
    return FindRightConst(LinearSearchForConst, Nodes);
  }

private:
  LinearSearcher LinearSearchForConst;
};

template <ConstPosition BadConstPosition, typename LinearSearcher>
BadConstFinder<BadConstPosition, LinearSearcher>
MakeBadConstFinder(const LinearSearcher &LinearSearchForConst) {
  return BadConstFinder<BadConstPosition, LinearSearcher>(LinearSearchForConst);
}

} // namespace voxel
} // namespace tidy
} // namespace clang
#endif // LLVM_CLANG_TOOLS_EXTRA_CLANG_TIDY_VOXEL_BAD_CONST_FINDER_H
