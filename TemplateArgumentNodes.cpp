#include "TemplateArgumentNodes.h"
#include "ASTMatchers.h"

namespace clang {
namespace ast_matchers {
internal::Matcher<TypeLoc> classTemplateArgumentContainingConst() {
  return typeLoc(isTemplateSpecializationLoc(),
                 forEach(typeLoc(anyOf(isConstNonPointerLoc(),
                                       findConstRefereeLocs(),
                                       findAllNonPointerConstPointeeLocs()))))
      .bind("template-specialization");
}

internal::Matcher<Stmt> functionTemplateArgumentContainingConst() {
  return declRefExpr(hasExplicitTemplateArgs(),
                     forEach(typeLoc(
                         anyOf(isConstNonPointerLoc(), findConstRefereeLocs(),
                               findAllNonPointerConstPointeeLocs()))))
      .bind("expr");
}
} // namespace ast_matchers
namespace tidy {
namespace voxel {

llvm::Optional<ClassTemplateArgumentNodes> ExtractClassTemplateArgumentNodes::
operator()(const ast_matchers::BoundNodes &BoundNodes) const {
  return MakeIfAllHaveValues<ClassTemplateArgumentNodes>(
      Extract<TemplateSpecializationTypeLoc>(BoundNodes,
                                             "template-specialization"),
      Extract<TypeLoc>(BoundNodes, "loc"));
}

llvm::Optional<FunctionTemplateArgumentNodes>
ExtractFunctionTemplateArgumentNodes::
operator()(const ast_matchers::BoundNodes &BoundNodes) const {
  return MakeIfAllHaveValues<FunctionTemplateArgumentNodes>(
      Extract<DeclRefExpr>(BoundNodes, "expr"),
      Extract<TypeLoc>(BoundNodes, "loc"));
}

LeftToRightSourceTraversal<RightOfNode<TypeLoc>, SourceLocation>
RightQualifierSpace(TypeLoc ArgumentLoc, DeclRefExpr const &Ancestor) {
  return LeftToRight(RightOf(ArgumentLoc), Ancestor.getRAngleLoc());
}

RightToLeftSourceTraversal<SourceLocation, SourceLocation>
LeftQualifierSpace(TypeLoc ArgumentLoc, DeclRefExpr const &Ancestor) {
  return RightToLeft(LeftOf(ArgumentLoc), Ancestor.getLAngleLoc());
}

LeftToRightSourceTraversal<RightOfNode<TypeLoc>, SourceLocation>
RightQualifierSpace(TypeLoc PointeeLoc,
                    TemplateSpecializationTypeLoc const &Ancestor) {
  return LeftToRight(RightOf(PointeeLoc), Ancestor.getRAngleLoc());
}

RightToLeftSourceTraversal<SourceLocation, SourceLocation>
LeftQualifierSpace(TypeLoc PointeeLoc,
                   TemplateSpecializationTypeLoc const &Ancestor) {
  return RightToLeft(LeftOf(PointeeLoc), Ancestor.getLAngleLoc());
}

} // namespace voxel
} // namespace tidy
} // namespace clang
